<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Tests\unit;

use Codeception\Test\Unit;
use Enuage\SchemaValidator\Constraint\FloatSchema;
use Enuage\SchemaValidator\Constraint\IntegerSchema;
use Enuage\SchemaValidator\Validator\SchemaValidator;
use UnitTester;

class NumberRangeValidationTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testMinimalFloatValue()
    {
        $schema = (new FloatSchema())->minimum(2.71);
        $this->assertFalse(SchemaValidator::validateData(2.7, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2.71, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2.72, $schema)->isValid());

        $schema = (new FloatSchema())->gte(2.71);
        $this->assertFalse(SchemaValidator::validateData(2.7, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2.71, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2.72, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testMinimalIntegerValue()
    {
        $schema = (new IntegerSchema())->minimum(20);
        $this->assertFalse(SchemaValidator::validateData(1, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(20, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(30, $schema)->isValid());

        $schema = (new IntegerSchema())->gte(20);
        $this->assertFalse(SchemaValidator::validateData(1, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(20, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(30, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testExclusiveMinimalFloatValue()
    {
        $schema = (new FloatSchema())->exclusiveMinimum(2.71);
        $this->assertFalse(SchemaValidator::validateData(2.70, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2.71, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2.720, $schema)->isValid());

        $schema = (new FloatSchema())->gt(2.71);
        $this->assertFalse(SchemaValidator::validateData(2.70, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2.71, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2.720, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testExclusiveMinimalIntegerValue()
    {
        $schema = (new IntegerSchema())->exclusiveMinimum(2);
        $this->assertFalse(SchemaValidator::validateData(1, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(30, $schema)->isValid());

        $schema = (new IntegerSchema())->gt(2);
        $this->assertFalse(SchemaValidator::validateData(1, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(30, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testMaximalFloatValue()
    {
        $schema = (new FloatSchema())->maximum(2.71);
        $this->assertTrue(SchemaValidator::validateData(2.700, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2.710, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2.72, $schema)->isValid());

        $schema = (new FloatSchema())->lte(2.71);
        $this->assertTrue(SchemaValidator::validateData(2.700, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2.710, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2.72, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testMaximalIntegerValue()
    {
        $schema = (new IntegerSchema())->maximum(-20);
        $this->assertTrue(SchemaValidator::validateData(-30, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(-20, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(10, $schema)->isValid());

        $schema = (new IntegerSchema())->lte(-20);
        $this->assertTrue(SchemaValidator::validateData(-30, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(-20, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(10, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testExclusiveMaximalFloatValue()
    {
        $schema = (new FloatSchema())->exclusiveMaximum(2.71);
        $this->assertTrue(SchemaValidator::validateData(2.700, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2.71, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2.72, $schema)->isValid());

        $schema = (new FloatSchema())->lt(2.71);
        $this->assertTrue(SchemaValidator::validateData(2.700, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2.71, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2.72, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testExclusiveMaximalIntegerValue()
    {
        $schema = (new IntegerSchema())->exclusiveMaximum(10);
        $this->assertTrue(SchemaValidator::validateData(-10, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(10, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(20, $schema)->isValid());

        $schema = (new IntegerSchema())->lt(10);
        $this->assertTrue(SchemaValidator::validateData(-10, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(10, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(20, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFloatRange()
    {
        $schema = (new FloatSchema())->range(1, 2);
        $this->assertFalse(SchemaValidator::validateData(0, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(1, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(1.5, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(3, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testIntegerRange()
    {
        $schema = (new IntegerSchema())->range(1, 3);
        $this->assertFalse(SchemaValidator::validateData(0, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(1, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(3, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(4, $schema)->isValid());
    }
}
