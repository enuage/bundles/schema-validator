<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Tests\unit;

use Codeception\Test\Unit;
use Enuage\SchemaValidator\Constraint\ArraySchema;
use Enuage\SchemaValidator\Constraint\AssociativeArraySchema;
use Enuage\SchemaValidator\Validator\SchemaValidator;
use UnitTester;

class ArrayValidationTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testUniqueItems()
    {
        $schema = (new ArraySchema())->uniqueItems();
        $this->assertTrue(SchemaValidator::validateData(['a', 'b', 'c'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a', 'b', 'a'], $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testRequiredItems()
    {
        $schema = (new ArraySchema())->required(['a', 'b']);
        $this->assertTrue(SchemaValidator::validateData(['a', 'b', 'c'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a', 'c'], $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testAssociativeArray()
    {
        $schema = new AssociativeArraySchema('assoc');
        $schema->addStringProperty('a')->required();
        $schema->addStringProperty('b')->required();
        $schema->addStringProperty('c');

        $this->assertTrue(SchemaValidator::validateData(['a' => 'a', 'b' => 'b', 'c' => 'c'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a' => 'a', 'c' => 'c'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a' => 'a', 'b' => 42], $schema)->isValid());

        $schema = (new ArraySchema('assoc'))->associative();
        $schema->addStringProperty('a')->required();
        $schema->addStringProperty('b')->required();
        $schema->addStringProperty('c');

        $this->assertTrue(SchemaValidator::validateData(['a' => 'a', 'b' => 'b', 'c' => 'c'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a' => 'a', 'c' => 'c'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a' => 'a', 'b' => 42], $schema)->isValid());
    }
}
