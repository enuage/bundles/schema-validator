<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Tests\unit;

use Codeception\Test\Unit;
use Enuage\SchemaValidator\Constraint\StringSchema;
use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Validator\SchemaValidator;
use UnitTester;

class StringValidationTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testMinLength()
    {
        $schema = (new StringSchema())->minLength(5);
        $this->assertTrue(SchemaValidator::validateData('Lorem', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('amet', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testMaxLength()
    {
        $schema = (new StringSchema())->maxLength(4);
        $this->assertTrue(SchemaValidator::validateData('true', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('false', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testRequiredLength()
    {
        $schema = (new StringSchema())->requiredLength(4);
        $this->assertTrue(SchemaValidator::validateData('true', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('false', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('no', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testEnum()
    {
        $enum = ['Lorem', 'ipsum', 'dolor', 'sit', 'amet'];
        $schema = (new StringSchema())->enum($enum);
        foreach ($enum as $item) {
            $this->assertTrue(SchemaValidator::validateData($item, $schema)->isValid());
        }

        $this->assertFalse(SchemaValidator::validateData('false', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatDateIso()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_DATE_ISO);
        $this->assertTrue(SchemaValidator::validateData('2020-05-12', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('2020.05.12', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatDateIsoOrdinal()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_DATE_ISO_ORDINAL);
        $this->assertTrue(SchemaValidator::validateData('2020-133', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('2020.133', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatDateTime()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_DATETIME);
        $this->assertTrue(SchemaValidator::validateData('2020-05-12 19:04:52', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('2020.05.12 19:04:52', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatDateIsoTime()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_DATETIME_ISO);
        $this->assertTrue(SchemaValidator::validateData('2020-05-12T19:04:52+03:00', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('2020.05.12T19:04:52+03:00', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatEmail()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_EMAIL);
        $this->assertTrue(SchemaValidator::validateData('spam312sn@gmail.com', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('test@localhost', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatIpAddress()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_IP);
        $this->assertTrue(SchemaValidator::validateData('127.0.0.1', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('1024.2048.4096.8192', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatIpV4Address()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_IP_V4);
        $this->assertTrue(SchemaValidator::validateData('127.0.0.1', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('1024.2048.4096.8192', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatIpV6Address()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_IP_V6);
        $this->assertTrue(SchemaValidator::validateData('0:0:0:0:0:0:0:1', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('0:0:0:0:0:0:0:fffff', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testPattern()
    {
        $schema = (new StringSchema())->pattern('/^asd$/i');
        $this->assertTrue(SchemaValidator::validateData('asd', $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData('ASD', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('dsa', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatTime()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_TIME);
        $this->assertTrue(SchemaValidator::validateData('04:20:00', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('4:20', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testG12HoursFormatTime()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_TIME_12);
        $this->assertTrue(SchemaValidator::validateData('4:20 AM', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('04:20', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testG12HoursFormatTimeWithSeconds()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_TIME_12_SECONDS);
        $this->assertTrue(SchemaValidator::validateData('4:20:00 AM', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('04:20', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testH24HoursFormatTime()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_TIME_24);
        $this->assertTrue(SchemaValidator::validateData('04:20', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('04:20 AM', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testH24HoursFormatTimeWithSeconds()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_TIME_24_SECONDS);
        $this->assertTrue(SchemaValidator::validateData('04:20:00', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('4:20:00 AM', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatTimestamp()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_TIMESTAMP);
        $this->assertTrue(SchemaValidator::validateData('1589299593', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('+1589299593', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatUri()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_URI);
        $this->assertTrue(SchemaValidator::validateData('https://www.php.net/', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('https:://www.php.net/', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatUuid()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_UUID);
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-1ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData('5321dc02-b8fd-11ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData('5321dc02-b8fd-21ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData('5321dc02-b8fd-31ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData('5321dc02-b8fd-41ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData('5321dc02-b8fd-51ea-b3de-0242ac130004', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatUuidV1()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_UUID_V1);
        $this->assertTrue(SchemaValidator::validateData('5321dc02-b8fd-11ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-21ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-31ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-41ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-51ea-b3de-0242ac130004', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatUuidV2()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_UUID_V2);
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-11ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData('5321dc02-b8fd-21ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-31ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-41ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-51ea-b3de-0242ac130004', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatUuidV3()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_UUID_V3);
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-11ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-21ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData('5321dc02-b8fd-31ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-41ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-51ea-b3de-0242ac130004', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatUuidV4()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_UUID_V4);
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-11ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-21ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-31ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData('5321dc02-b8fd-41ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-51ea-b3de-0242ac130004', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatUuidV5()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_UUID_V5);
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-11ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-21ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-31ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('5321dc02-b8fd-41ea-b3de-0242ac130004', $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData('5321dc02-b8fd-51ea-b3de-0242ac130004', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatDateWeek()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_WEEK_ISO);
        $this->assertTrue(SchemaValidator::validateData('2020-W20', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('2020.W20', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFormatDateWeekIsoWeekday()
    {
        $schema = (new StringSchema())->format(AbstractFormatConstraint::FORMAT_WEEK_ISO_WEEKDAY);
        $this->assertTrue(SchemaValidator::validateData('2020-W20-2', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('2020-W20-8', $schema)->isValid());
    }
}
