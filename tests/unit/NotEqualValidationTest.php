<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Tests\unit;

use Codeception\Test\Unit;
use Enuage\SchemaValidator\Constraint\BooleanSchema;
use Enuage\SchemaValidator\Constraint\FloatSchema;
use Enuage\SchemaValidator\Constraint\IntegerSchema;
use Enuage\SchemaValidator\Constraint\StringSchema;
use Enuage\SchemaValidator\Validator\SchemaValidator;
use UnitTester;

class NotEqualValidationTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testString()
    {
        $schema = (new StringSchema())->notEqualTo('Hello world');
        $this->assertTrue(SchemaValidator::validateData('Lorem ipsum dolor sit amet', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('Hello world', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testInteger()
    {
        $schema = (new IntegerSchema())->notEqualTo(42);
        $this->assertTrue(SchemaValidator::validateData(73, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(42, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testFloat()
    {
        $schema = (new FloatSchema())->notEqualTo(3.14);
        $this->assertTrue(SchemaValidator::validateData(2.71, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(3.14, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testBoolean()
    {
        $schema = (new BooleanSchema())->notEqualTo(false);
        $this->assertTrue(SchemaValidator::validateData(true, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(false, $schema)->isValid());
    }
}
