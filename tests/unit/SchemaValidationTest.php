<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Tests\unit;

use ArrayObject;
use Codeception\Test\Unit;
use Enuage\SchemaValidator\Constraint\ArraySchema;
use Enuage\SchemaValidator\Constraint\BooleanSchema;
use Enuage\SchemaValidator\Constraint\FloatSchema;
use Enuage\SchemaValidator\Constraint\IntegerSchema;
use Enuage\SchemaValidator\Constraint\JsonSchema;
use Enuage\SchemaValidator\Constraint\NumberSchema;
use Enuage\SchemaValidator\Constraint\ObjectSchema;
use Enuage\SchemaValidator\Constraint\ScalarSchema;
use Enuage\SchemaValidator\Constraint\Schema;
use Enuage\SchemaValidator\Constraint\SchemaPrototype;
use Enuage\SchemaValidator\Constraint\StringSchema;
use Enuage\SchemaValidator\Validator\SchemaValidator;
use InvalidArgumentException;
use stdClass;
use UnitTester;

use function json_decode;

class SchemaValidationTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testUnsupportedDataType()
    {
        foreach ([null, 1, '', false] as $data) {
            $this->tester->expectThrowable(
                InvalidArgumentException::class,
                function () use ($data) {
                    SchemaValidator::validateData($data, new Schema('unsupported'));
                }
            );
        }
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testCountMultiplicityValidation()
    {
        $schema = (new Schema('count'))->multipleOf(2);
        $this->assertTrue(SchemaValidator::validateData(['a', 'b'], $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(['a', 'b', 'c', 'd'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a', 'b', 'c'], $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a":1,"b":2}'), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a":1,"b":2,"c":3,"d":4}'), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(json_decode('{"a":1,"b":2,"c":3}'), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(new ArrayObject(['a', 'b']), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(new ArrayObject(['a', 'b', 'c', 'd']), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(new ArrayObject(['a', 'b', 'c']), $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testCountMinimalItemsValidation()
    {
        $schema = (new ArraySchema())->minItems(2);
        $this->assertTrue(SchemaValidator::validateData(['a', 'b'], $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(['a', 'b', 'c'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a'], $schema)->isValid());

        $schema = (new ObjectSchema())->additionalProperties(true)->minProperties(2);
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a": 1, "b": 2}'), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a": 1, "b": 2, "c": 3}'), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(json_decode('{"a": 1}'), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(new ArrayObject(['a', 'b']), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(new ArrayObject(['a']), $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testCountMaximalPropertiesValidation()
    {
        $schema = (new ArraySchema())->maxItems(2);
        $this->assertTrue(SchemaValidator::validateData(['a'], $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(['a', 'b'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a', 'b', 'c'], $schema)->isValid());

        $schema = (new ObjectSchema())->additionalProperties(true)->maxProperties(2);
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a":1}'), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a":1, "b":2}'), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(json_decode('{"a":1, "b":2, "c":3}'), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(new ArrayObject(['a', 'b']), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(new ArrayObject(['a', 'b', 'c']), $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testNotEmptyValidation()
    {
        $schema = (new Schema('notEmpty'))->notEmpty();
        $this->assertTrue(SchemaValidator::validateData(['a'], $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(new ArrayObject(['a']), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a":1}'), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData([], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(new ArrayObject(), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(new stdClass(), $schema)->isValid());

        $schema = (new ArraySchema())->notEmpty();
        $this->assertTrue(SchemaValidator::validateData(['a'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData([], $schema)->isValid());

        $schema = (new ObjectSchema())->additionalProperties(true)->notEmpty();
        $this->assertTrue(SchemaValidator::validateData(new ArrayObject(['a']), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a":1}'), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(new ArrayObject(), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(new stdClass(), $schema)->isValid());

        $schema = (new StringSchema())->notEmpty();
        $this->assertTrue(SchemaValidator::validateData('Lorem ipsum dolor sit amet', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testTypesValidation()
    {
        $schema = (new ScalarSchema());
        $this->assertTrue(SchemaValidator::validateData('Lorem ipsum dolor sit amet', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(new stdClass(), $schema)->isValid());

        $schema = (new StringSchema());
        $this->assertTrue(SchemaValidator::validateData('Lorem ipsum dolor sit amet', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(3.14, $schema)->isValid());

        $schema = (new NumberSchema());
        $this->assertTrue(SchemaValidator::validateData(3.14, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData([], $schema)->isValid());

        $schema = (new IntegerSchema());
        $this->assertTrue(SchemaValidator::validateData(42, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(3.14, $schema)->isValid());

        $schema = (new FloatSchema());
        $this->assertTrue(SchemaValidator::validateData(3.14, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('Lorem ipsum dolor sit amet', $schema)->isValid());

        $schema = (new BooleanSchema());
        $this->assertTrue(SchemaValidator::validateData(true, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(null, $schema)->isValid());

        $schema = (new SchemaPrototype(['string']));
        $this->assertTrue(SchemaValidator::validateData('Lorem ipsum dolor sit amet', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(3.14, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testRequiredProperties()
    {
        $schema = new JsonSchema();
        $schema->addStringProperty('a')->required();
        $schema->addStringProperty('b')->required();

        $this->assertTrue(SchemaValidator::validateData('{"a":"exists", "b":"exists"}', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('{"a":"exists"}', $schema)->isValid());

        $schema = new JsonSchema();
        $schema->addStringProperty('a');
        $schema->addStringProperty('b');
        $schema->required(['a', 'b']);

        $this->assertTrue(SchemaValidator::validateData('{"a":"exists", "b":"exists"}', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('{"a":"exists"}', $schema)->isValid());

        $schema = new JsonSchema();
        $schema->required(['a', 'b']);

        $this->assertTrue(SchemaValidator::validateData('{"a":"exists", "b":"exists"}', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('{"a":"exists"}', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testAllowedArrayItems()
    {
        $schema = (new ArraySchema())->items(['lorem', 'ipsum', 'dolor']);
        $this->assertTrue(SchemaValidator::validateData(['lorem'], $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(['lorem', 'dolor', 'ipsum'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['sit', 'amet', 'ipsum'], $schema)->isValid());

        $schema = (new ArraySchema())->items(
            [
                (new StringSchema())->setPropertyName('lorem'),
                (new StringSchema())->setPropertyName('ipsum'),
                (new StringSchema())->setPropertyName('dolor'),
            ]
        );
        $this->assertTrue(SchemaValidator::validateData(['lorem'], $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(['lorem', 'dolor', 'ipsum'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['sit', 'amet', 'ipsum'], $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testAllowedObjectItems()
    {
        $schema = (new ObjectSchema())->items(
            [
                (new FloatSchema())->setPropertyName('lorem')->notEqualTo(3.14),
                (new StringSchema())->setPropertyName('ipsum')->requiredLength(5),
                (new IntegerSchema())->setPropertyName('dolor')->range(1, 100),
            ]
        );
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"lorem":2.71}'), $schema)->isValid());
        $this->assertTrue(
            SchemaValidator::validateData(json_decode('{"lorem":1.25, "dolor":42, "ipsum":"lorem"}'), $schema)->isValid(
            )
        );
        $this->assertFalse(
            SchemaValidator::validateData(
                json_decode('{"sit":3.14, "amet":"Hello world", "ipsum":"lorem"}'),
                $schema
            )->isValid()
        );
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testAllOf()
    {
        $schema = (new Schema())->allOf('string');
        $this->assertTrue(SchemaValidator::validateData(['a', 'b', 'c'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a', 'b', 3.14], $schema)->isValid());

        $schema = (new Schema())->allOf((new StringSchema())->requiredLength(5));
        $this->assertTrue(SchemaValidator::validateData(['lorem', 'ipsum', 'dolor'], $schema)->isValid());
        $this->assertFalse(
            SchemaValidator::validateData(['lorem', 'ipsum', 'dolor', 'sit', 'amet'], $schema)->isValid()
        );
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testOneOf()
    {
        $schema = (new Schema())->oneOf('string');
        $this->assertTrue(SchemaValidator::validateData(['a'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['a', 3.14], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData([3.14], $schema)->isValid());

        $schema = (new Schema())->oneOf((new StringSchema())->requiredLength(5));
        $this->assertTrue(SchemaValidator::validateData(['lorem'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['lorem', 'amet'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['amet'], $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testAdditionalProperties()
    {
        $schema = (new ObjectSchema())->additionalProperties(false);
        $this->assertTrue(SchemaValidator::validateData(json_decode('{}'), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(json_decode('{"a":"b"}'), $schema)->isValid());

        $schema = (new ObjectSchema())->additionalProperties(true);
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a":"b"}'), $schema)->isValid());

        $schema = (new ObjectSchema());
        $schema->addStringProperty('a');
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a":"b"}'), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(json_decode('{"a":"b", "c":"d"}'), $schema)->isValid());

        $schema = (new ObjectSchema())->additionalProperties(['a']);
        $this->assertTrue(SchemaValidator::validateData(json_decode('{}'), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a":"b"}'), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(json_decode('{"c":"d"}'), $schema)->isValid());

        $additionalProperty = new StringSchema();
        $additionalProperty->setPropertyName('a');
        $schema = (new ObjectSchema())->additionalProperties([$additionalProperty]);
        $this->assertTrue(SchemaValidator::validateData(json_decode('{}'), $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(json_decode('{"a":"b"}'), $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(json_decode('{"c":"d"}'), $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testRecursiveValidation()
    {
        $book = new ObjectSchema('book');
        $book->addStringProperty('author')->required();

        $chapter = new ObjectSchema('chapter');
        $chapter->addStringProperty('title')->required();
        $chapter->addIntegerProperty('number')->minimum(0)->required();

        $page = new ObjectSchema('page');
        $page->addIntegerProperty('number')->minimum(1)->required();

        $chapter->addArrayProperty('pages')->allOf($page);
        $book->addArrayProperty('chapters')->allOf($chapter);

        $this->assertTrue(SchemaValidator::validateData(
            json_decode('{
            "author": "John Doe",
            "chapters": [
                {
                    "title": "Hello world",
                    "number": 1,
                    "pages": [
                        {
                            "number": 1
                        }
                    ]
                }
            ]
        }'), $book)->isValid());

        $result = SchemaValidator::validateData(
            json_decode('{"author":"John Doe","chapters":[{"title":22,"number":-1,"pages":{"number":0}}]}'),
            $book
        );

        $this->assertFalse($result->isValid());
    }
}
