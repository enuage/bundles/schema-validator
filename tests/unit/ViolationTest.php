<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Tests\unit;

use Codeception\Test\Unit;
use Enuage\SchemaValidator\Constraint\ObjectSchema;
use Enuage\SchemaValidator\Validator\SchemaValidator;
use UnitTester;

use function json_decode;

class ViolationTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testViolationMessage()
    {
        $schema = new ObjectSchema('invalid');
        $schema->addObjectProperty('1_level')->addObjectProperty('2_level')->addStringProperty('test');

        $result = SchemaValidator::validateData(json_decode('{"1_level":{"2_level": {"test":42}}}'), $schema);
        $this->assertEquals('invalid/1_level/2_level/test', $result->getViolations()->first()->getPointer());

        $schema = new ObjectSchema('invalid');
        $schema->addArrayProperty('items')->allOf('string');
        $result = SchemaValidator::validateData(json_decode('{"items":["a", "b", 3]}'), $schema);
        $this->assertEquals('invalid/items/2', $result->getViolations()->first()->getPointer());

        $objSchema = new ObjectSchema();
        $objSchema->addStringProperty('test');
        $schema = new ObjectSchema('invalid');
        $schema->addArrayProperty('items')->allOf($objSchema);
        $result = SchemaValidator::validateData(json_decode('{"items":[{"test":"a"}, {"test":"b"}, {"test":3}]}'), $schema);
        $this->assertEquals('invalid/items/2/test', $result->getViolations()->first()->getPointer());
    }
}
