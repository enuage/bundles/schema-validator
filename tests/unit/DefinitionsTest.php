<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Tests\unit;

use Codeception\Test\Unit;
use Enuage\SchemaValidator\Constraint\ArraySchema;
use Enuage\SchemaValidator\Constraint\AssociativeArraySchema;
use Enuage\SchemaValidator\Constraint\StringSchema;
use Enuage\SchemaValidator\Validator\SchemaValidator;
use UnitTester;

class DefinitionsTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testItemsSchemaByDefinition()
    {
        $schema = new ArraySchema();
        $stringDefinition = new StringSchema();
        $stringDefinition->requiredLength(5);
        $schema->addDefinition('test', $stringDefinition);
        $schema->allOf('test');

        $this->assertEquals($schema->getItemsSchema(), $stringDefinition);

        $this->assertTrue(SchemaValidator::validateData(['lorem', 'ipsum', 'dolor'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['lorem', 'ipsum', 'dolor', 'sit', 'amet'], $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testAddByReference()
    {
        $schema = new AssociativeArraySchema();

        $stringDefinition = new StringSchema();
        $stringDefinition->requiredLength(5);
        $schema->addDefinition('test', $stringDefinition);

        $schema->addByReference('t1', 'test');
        $schema->addByReference('t2', 'test');

        $this->assertEquals($schema->getProperties()->get('t1'), $stringDefinition);
        $this->assertEquals($schema->getProperties()->get('t2'), $stringDefinition);

        $this->assertTrue(SchemaValidator::validateData(['t1' => 'lorem', 't2' => 'ipsum'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['t1' => 'amet', 't2' => 'ipsum'], $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(['t1' => 'lorem', 't2' => 'amet'], $schema)->isValid());
    }
}
