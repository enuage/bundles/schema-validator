<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Tests\unit;

use Codeception\Test\Unit;
use Enuage\SchemaValidator\Constraint\FloatSchema;
use Enuage\SchemaValidator\Constraint\IntegerSchema;
use Enuage\SchemaValidator\Constraint\NumberSchema;
use Enuage\SchemaValidator\Validator\SchemaValidator;
use UnitTester;

class PositiveNumberValidationTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testPositiveNumber()
    {
        $schema = (new NumberSchema())->positive();
        $this->assertTrue(SchemaValidator::validateData(0, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(0.0, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(1, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2.71, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(-1, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(-1.1, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testNegativeNumber()
    {
        $schema = (new NumberSchema())->negative();
        $this->assertTrue(SchemaValidator::validateData(0, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(0.0, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(-1, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(-1.1, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2.71, $schema)->isValid());
    }


    /** @noinspection PhpUnhandledExceptionInspection */
    public function testPositiveFloat()
    {
        $schema = (new FloatSchema())->positive();
        $this->assertTrue(SchemaValidator::validateData(0.0, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(2.71, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(-1.1, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testNegativeFloat()
    {
        $schema = (new FloatSchema())->negative();
        $this->assertTrue(SchemaValidator::validateData(0.0, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(-1.1, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(2.71, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testPositiveInteger()
    {
        $schema = (new IntegerSchema())->positive();
        $this->assertTrue(SchemaValidator::validateData(0, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(1, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(-1, $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testNegativeInteger()
    {
        $schema = (new IntegerSchema())->negative();
        $this->assertTrue(SchemaValidator::validateData(0, $schema)->isValid());
        $this->assertTrue(SchemaValidator::validateData(-1, $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData(1, $schema)->isValid());
    }
}
