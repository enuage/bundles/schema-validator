<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Tests\unit;

use Codeception\Test\Unit;
use Enuage\SchemaValidator\Constraint\AssociativeArraySchema;
use Enuage\SchemaValidator\Constraint\JsonSchema;
use Enuage\SchemaValidator\Constraint\ObjectSchema;
use Enuage\SchemaValidator\Constraint\PropertySchema;
use Enuage\SchemaValidator\Constraint\SchemaInterface;
use Enuage\SchemaValidator\Constraint\StringSchema;
use Enuage\SchemaValidator\Validator\SchemaValidator;
use Exception;
use UnitTester;

class JsonValidationTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testJsonValidation()
    {
        $schema = (new JsonSchema())->additionalProperties(true);
        $this->assertTrue(SchemaValidator::validateData('{"a":"b"}', $schema)->isValid());
        $this->assertFalse(SchemaValidator::validateData('Hello world', $schema)->isValid());
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public function testSchemaImport()
    {
        $json = <<<JSON
{
    "title": "test",
    "properties": {
        "abstract": {
            "types": ["string", "integer"]
        },
        "string": {
            "type": "string",
            "requiredLength": 5,
            "enum": ["Lorem", "ipsum"],
            "notEmpty": true
        },
        "integer": {
            "type": "integer",
            "maximum": 42,
            "minimum": 2,
            "positive": true,
            "required": true
        },
        "float": {
            "type": "float",
            "range": [2.71, 3.14]
        },
        "array" : {
            "type": "array",
            "items": ["Lorem", "ipsum", "dolor", "sit", "amet"],
            "maxItems": 3,
            "minItems": 1,
            "allOf": "string"
        },
        "object": {
            "type": "object",
            "maxProperties": 3,
            "minProperties": 2,
            "required": ["lorem", "ipsum"],
            "properties": {
                "lorem": { "type": "string" }, 
                "ipsum": { "type": "string" }, 
                "dolor": {
                    "title": "aveRecursion",
                    "type": "array",
                    "associative": true,
                    "maxItems": 3,
                    "minItems": 2,
                    "required": ["lorem", "ipsum"],
                    "properties": {
                        "lorem": { "type": "string" }, 
                        "ipsum": { "type": "string" }, 
                        "dolor": { "type": "string" }, 
                        "sit": { "type": "string" },
                        "amet": { "type": "string" }
                    }
                }, 
                "sit": { "type": "string" },
                "amet": { "type": "string" }
            }
        },
        "associativeArray": {
            "type": "array",
            "associative": true,
            "maxItems": 3,
            "minItems": 2,
            "required": ["lorem", "ipsum"],
            "properties": {
                "lorem": { "type": "string" }, 
                "ipsum": { "type": "string" }, 
                "dolor": { "type": "string" }, 
                "sit": { "type": "string" },
                "amet": { "type": "string" }
            }
        },
        "testReference": {
            "reference": "testDefinition"
        },
        "testRef": {
            "\$ref": "testDefinition"
        }
    },
    "definitions": {
        "testDefinition": {
            "type": "string",
            "requiredLength": 5
        }
    }
}
JSON;

        $expectedSchema = new JsonSchema('test');
        $expectedSchema->addProperty((new PropertySchema(['string', 'integer']))->setPropertyName('abstract'));
        $expectedSchema->addStringProperty('string')->requiredLength(5)->notEmpty()->enum(['Lorem', 'ipsum']);
        $expectedSchema->addIntegerProperty('integer')->maximum(42)->minimum(2)->positive()->required(true);
        $expectedSchema->addFloatProperty('float')->range(2.71, 3.14);
        $expectedSchema->addArrayProperty('array')->minItems(1)->maxItems(3)->items(
            ['Lorem', 'ipsum', 'dolor', 'sit', 'amet']
        )->allOf('string');

        $object = new ObjectSchema('object', $expectedSchema);
        $object->maxProperties(3);
        $object->minProperties(2);
        $object->required(['lorem', 'ipsum']);
        $object->addStringProperty('lorem');
        $object->addStringProperty('ipsum');
        $object->addProperty($this->getAssociativeArray('aveRecursion', $object));
        $object->addStringProperty('sit');
        $object->addStringProperty('amet');
        $expectedSchema->addProperty($object);

        $expectedSchema->addProperty($this->getAssociativeArray('associativeArray', $expectedSchema));

        $definition = new StringSchema();
        $definition->requiredLength(5);
        $expectedSchema->addDefinition('testDefinition', $definition);

        $expectedSchema->addByReference('testReference', 'testDefinition');
        $expectedSchema->addByReference('testRef', 'testDefinition');

        $this->assertEquals($expectedSchema, JsonSchema::fromString($json));
    }

    /**
     * @param string $title
     * @param SchemaInterface $parent
     *
     * @return AssociativeArraySchema
     *
     * @throws Exception
     */
    private function getAssociativeArray(string $title, SchemaInterface $parent): AssociativeArraySchema
    {
        $associativeArray = new AssociativeArraySchema($title, $parent);
        $associativeArray->maxItems(3);
        $associativeArray->minItems(2);
        $associativeArray->required(['lorem', 'ipsum']);
        $associativeArray->addStringProperty('lorem');
        $associativeArray->addStringProperty('ipsum');
        $associativeArray->addStringProperty('dolor');
        $associativeArray->addStringProperty('sit');
        $associativeArray->addStringProperty('amet');

        return $associativeArray;
    }
}
