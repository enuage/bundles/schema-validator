<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Throwable;

use Exception;

class SchemaValidationException extends Exception
{
}
