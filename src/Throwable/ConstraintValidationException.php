<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Throwable;

use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Element\ChainNode;
use Exception;

use function array_reverse;
use function implode;
use function is_array;
use function is_object;
use function json_encode;
use function str_replace;
use function strval;

class ConstraintValidationException extends SchemaValidationException
{
    /**
     * @var string|null
     */
    protected $pointer;

    /**
     * ValidationException constructor.
     *
     * @param ValidatableProperty $property
     * @param ValidationConstraint $constraint
     * @param string|null $type
     *
     * @throws Exception
     */
    public function __construct(ValidatableProperty $property, ValidationConstraint $constraint, string $type = null)
    {
        $pointerNodes = $property->getPointerChain()->getNodes()->map(function (ChainNode $node) {
            return $node->getValue();
        })->getValues();

        $pointer = implode('/', array_reverse($pointerNodes));

        $message = $constraint->getErrorMessage($type);
        $message = str_replace('{propertyName}', $pointer, $message);

        $propertyValue = $property->getValue();
        if (is_array($propertyValue) || is_object($propertyValue)) {
            $propertyValue = json_encode($propertyValue);
        }
        $message = str_replace('{propertyValue}', strval($propertyValue), $message);

        $message = str_replace('{propertyType}', $property->getType(), $message);

        $this->pointer = $pointer;

        parent::__construct($message);
    }

    /**
     * @return string|null
     */
    public function getPointer()
    {
        return $this->pointer;
    }
}
