<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Parser;

use Enuage\SchemaValidator\Constraint\AbstractSchema;
use Enuage\SchemaValidator\Constraint\ArraySchema;
use Enuage\SchemaValidator\Constraint\AssociativeArraySchema;
use Enuage\SchemaValidator\Constraint\AssociativeSchema;
use Enuage\SchemaValidator\Constraint\FloatSchema;
use Enuage\SchemaValidator\Constraint\IntegerSchema;
use Enuage\SchemaValidator\Constraint\JsonSchema;
use Enuage\SchemaValidator\Constraint\NumberSchema;
use Enuage\SchemaValidator\Constraint\ObjectSchema;
use Enuage\SchemaValidator\Constraint\PropertySchema;
use Enuage\SchemaValidator\Constraint\Schema;
use Enuage\SchemaValidator\Constraint\StringSchema;
use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\Validation\AbstractLengthConstraint;
use Enuage\SchemaValidator\Constraint\Validation\EnumConstraint;
use Enuage\Type\AdvancedArrayObject;
use Exception;
use RuntimeException;
use function array_filter;
use function array_key_exists;
use function array_values;
use function count;
use function floatval;
use function intval;
use function is_array;
use function is_scalar;
use function is_string;
use function strval;

class SchemaParser
{
    const PROPERTY_TITLE = 'title';
    const PROPERTY_PROPERTIES = 'properties';

    const CONFIGURATION_TYPE = 'type';
    const CONFIGURATION_TYPES = 'types';
    const CONFIGURATION_REQUIRED = 'required';
    const CONFIGURATION_NOT_EMPTY = 'notEmpty';

    const CONFIGURATION_NEQ = 'neq'; // Alias for `notEqual`
    const CONFIGURATION_NOT_EQUAL = 'notEqual';

    const CONFIGURATION_FORMAT = 'format';
    const CONFIGURATION_PATTERN = AbstractFormatConstraint::FORMAT_PATTERN;
    const CONFIGURATION_ENUM = EnumConstraint::CONSTRAINT_NAME;
    const CONFIGURATION_LENGTH_REQUIRED = AbstractLengthConstraint::LENGTH_REQUIRED;
    const CONFIGURATION_LENGTH_MINIMUM = AbstractLengthConstraint::LENGTH_MINIMUM;
    const CONFIGURATION_LENGTH_MAXIMUM = AbstractLengthConstraint::LENGTH_MAXIMUM;

    const CONFIGURATION_POSITIVE = 'positive';
    const CONFIGURATION_NEGATIVE = 'negative';
    const CONFIGURATION_GT = 'gt'; // Alias for `exclusiveMinimum`
    const CONFIGURATION_MINIMUM_EXCLUSIVE = 'exclusiveMinimum';
    const CONFIGURATION_GTE = 'gte'; // Alias for `minimum`
    const CONFIGURATION_MINIMUM = 'minimum';
    const CONFIGURATION_LT = 'lt'; // Alias for `exclusiveMaximum`
    const CONFIGURATION_MAXIMUM_EXCLUSIVE = 'exclusiveMaximum';
    const CONFIGURATION_LTE = 'lte'; // Alias for `maximum`
    const CONFIGURATION_MAXIMUM = 'maximum';
    const CONFIGURATION_RANGE = 'range';

    const CONFIGURATION_ITEMS_MAX = 'maxItems';
    const CONFIGURATION_ITEMS_MIN = 'minItems';
    const CONFIGURATION_ITEMS_UNIQUE = 'uniqueItems';
    const CONFIGURATION_ASSOCIATIVE = 'associative';

    const CONFIGURATION_PROPERTIES_MAX = 'maxProperties';
    const CONFIGURATION_PROPERTIES_MIN = 'minProperties';

    const CONFIGURATION_ITEMS = 'items';
    const CONFIGURATION_DEFINITIONS = 'definitions';
    const CONFIGURATION_MULTIPLE_OF = 'multipleOf';
    const CONFIGURATION_SCHEMA_REQUIRED = 'requiredProperty';
    const CONFIGURATION_SCHEMA_ONE_OF = 'oneOf';
    const CONFIGURATION_SCHEMA_ALL_OF = 'allOf';

    const CONFIGURATION_REFERENCE = 'reference';
    const CONFIGURATION_REF = '$ref';

    /**
     * @param Schema $schema
     * @param $configuration
     *
     * @return ArraySchema|AssociativeSchema|JsonSchema|Schema
     *
     * @throws Exception
     */
    public static function fromArray(Schema $schema, $configuration)
    {
        if (is_array($configuration)) {
            $configuration = new AdvancedArrayObject($configuration);
        }

        if ($configuration instanceof AdvancedArrayObject) {
            self::applySchemaDefault($schema, $configuration);

            return self::importProperties($schema, $configuration->get(static::PROPERTY_PROPERTIES, []));
        }

        return $schema;
    }

    /**
     * @param Schema $schema
     * @param AdvancedArrayObject $configuration
     *
     * @return Schema
     *
     * @throws Exception
     */
    protected static function applySchemaDefault(Schema $schema, AdvancedArrayObject $configuration): Schema
    {
        if ($configuration->containsKey(static::CONFIGURATION_DEFINITIONS)) {
            $definitions = $configuration->get(static::CONFIGURATION_DEFINITIONS);
            if (!is_array($definitions)) {
                throw new RuntimeException('Property `definitions` should be of type "array".');
            }

            $definitionsSchemas = [];
            foreach ($definitions as $indexName => $definition) {
                $definitionsSchemas[$indexName] = self::createProperty($schema, $definition);
            }

            $schema->definitions($definitionsSchemas);
        }

        if ($configuration->containsKey(static::CONFIGURATION_ITEMS)) {
            $items = $configuration->get(static::CONFIGURATION_ITEMS);
            if (!is_array($items)) {
                throw new RuntimeException('Property `items` should be of type "array".');
            }

            if ($schema instanceof ArraySchema) {
                $schema->items(array_filter(array_values($items), function ($item) {
                    return is_scalar($item);
                }));
            } else {
                $itemsSchemas = [];
                foreach ($items as $indexName => $item) {
                    $itemsSchemas[$indexName] = self::createProperty($schema, $item);
                }

                $schema->items($itemsSchemas);
            }
        }

        if ($multipleOf = $configuration->getIntValue(static::CONFIGURATION_MULTIPLE_OF)) {
            $schema->multipleOf($multipleOf);
        }

        if ($configuration->containsKey(static::CONFIGURATION_NOT_EMPTY)) {
            $schema->notEmpty($configuration->getBooleanValue(static::CONFIGURATION_NOT_EMPTY));
        }

        if ($configuration->containsKey(static::CONFIGURATION_SCHEMA_REQUIRED)) {
            $schema->setRequired($configuration->getBooleanValue(static::CONFIGURATION_SCHEMA_REQUIRED));
        }

        if ($configuration->containsKey(static::CONFIGURATION_SCHEMA_REQUIRED)) {
            $schema->setRequired($configuration->getBooleanValue(static::CONFIGURATION_SCHEMA_REQUIRED));
        }

        if ($configuration->containsKey(static::CONFIGURATION_SCHEMA_ALL_OF)) {
            $schema->allOf(
                self::getConstraintOrString($schema, $configuration->get(static::CONFIGURATION_SCHEMA_ALL_OF))
            );
        }

        if ($configuration->containsKey(static::CONFIGURATION_SCHEMA_ONE_OF)) {
            $schema->oneOf(
                self::getConstraintOrString($schema, $configuration->get(static::CONFIGURATION_SCHEMA_ONE_OF))
            );
        }

        if ($configuration->containsKey(static::CONFIGURATION_REQUIRED)) {
            $schema->required(array_filter(
                array_values($configuration->get(static::CONFIGURATION_REQUIRED, [])),
                function ($requiredProperty) {
                    return is_string($requiredProperty);
                }
            ));
        }

        return $schema;
    }

    /**
     * @param Schema $parent
     * @param array $configuration
     * @param string|null $propertyName
     *
     * @return AbstractSchema
     *
     * @throws Exception
     */
    protected static function createProperty(
        Schema $parent,
        array $configuration,
        string $propertyName = null
    ): AbstractSchema {
        $configuration = new AdvancedArrayObject($configuration);

        if ($configuration->containsKey(static::CONFIGURATION_TYPES)) {
            $property = new PropertySchema($configuration->get(static::CONFIGURATION_TYPES));
        } elseif ($configuration->containsKey(static::CONFIGURATION_TYPE)) {
            $type = $configuration->get(static::CONFIGURATION_TYPE);

            if (
                $type === ArraySchema::getName() &&
                true === $configuration->getBooleanValue(static::CONFIGURATION_ASSOCIATIVE)
            ) {
                $type = AssociativeArraySchema::getName();
            }

            $property = $parent->getConstraintsFactory()->createConstraint($type, $parent);

            switch ($property::getName()) {
                case StringSchema::getName():
                    /** @var StringSchema $property */
                    $property = self::applyString($property, $configuration);
                    break;
                case NumberSchema::getName():
                case IntegerSchema::getName():
                case FloatSchema::getName():
                    /** @var NumberSchema $property */
                    $property = self::applyNumber($property, $configuration);
                    break;
                case ArraySchema::getName():
                    /** @var ArraySchema $property */
                    $property = self::applyArray($property, $configuration);
                    break;
                case AssociativeArraySchema::getName():
                case ObjectSchema::getName():
                case JsonSchema::getName():
                    /** @var ObjectSchema $property */
                    $property = self::applyObject($property, $configuration);
                    break;
            }
        } else {
            throw new RuntimeException(
                sprintf(
                    'Configuration must contain one of the following configurations: "%s".',
                    implode('", "', [static::CONFIGURATION_TYPES, static::CONFIGURATION_TYPE])
                )
            );
        }

        $propertyName = $configuration->getStringValue(static::PROPERTY_TITLE, $propertyName);
        if (null !== $propertyName) {
            $property->setPropertyName($propertyName);
        }

        if (false === ($property instanceof Schema)) {
            if ($configuration->containsKey(static::CONFIGURATION_REQUIRED)) {
                $property->required($configuration->getBooleanValue(static::CONFIGURATION_REQUIRED));
            }
        }

        return $property;
    }

    /**
     * @param StringSchema $property
     * @param AdvancedArrayObject $configuration
     *
     * @return StringSchema
     * @throws Exception
     */
    protected static function applyString(StringSchema $property, AdvancedArrayObject $configuration): StringSchema
    {
        $properties = [
            static::CONFIGURATION_NEQ => 'notEqualTo',
            static::CONFIGURATION_NOT_EQUAL => 'notEqualTo',
            static::CONFIGURATION_FORMAT => 'format',
            static::CONFIGURATION_PATTERN => 'pattern',
        ];

        foreach ($properties as $configurationName => $method) {
            if ($value = $configuration->getStringValue($configurationName)) {
                $property->$method($value);
            }
        }

        $properties = [
            static::CONFIGURATION_LENGTH_REQUIRED => 'requiredLength',
            static::CONFIGURATION_LENGTH_MINIMUM => 'minLength',
            static::CONFIGURATION_LENGTH_MAXIMUM => 'maxLength',
        ];

        foreach ($properties as $configurationName => $method) {
            if ($value = $configuration->getIntValue($configurationName)) {
                $property->$method($value);
            }
        }

        if ($configuration->containsKey(static::CONFIGURATION_NOT_EMPTY)) {
            $property->notEmpty($configuration->getBooleanValue(static::CONFIGURATION_NOT_EMPTY));
        }

        if ($configuration->containsKey(static::CONFIGURATION_ENUM)) {
            $property->enum(array_values($configuration->get(static::CONFIGURATION_ENUM)));
        }

        return $property;
    }

    /**
     * @param NumberSchema|IntegerSchema|FloatSchema $property
     * @param AdvancedArrayObject $configuration
     *
     * @return NumberSchema
     *
     * @throws Exception
     */
    protected static function applyNumber(NumberSchema $property, AdvancedArrayObject $configuration): NumberSchema
    {
        if (true === $configuration->getBooleanValue(static::CONFIGURATION_POSITIVE)) {
            $property->positive();
        }

        if (true === $configuration->getBooleanValue(static::CONFIGURATION_NEGATIVE)) {
            $property->negative();
        }

        $isInteger = $property instanceof IntegerSchema;
        if ($isInteger || $property instanceof FloatSchema) {
            $properties = [
                static::CONFIGURATION_NEQ => 'notEqualTo',
                static::CONFIGURATION_NOT_EQUAL => 'notEqualTo',
                static::CONFIGURATION_GT => 'exclusiveMinimum',
                static::CONFIGURATION_GTE => 'minimum',
                static::CONFIGURATION_MINIMUM_EXCLUSIVE => 'exclusiveMinimum',
                static::CONFIGURATION_MINIMUM => 'minimum',
                static::CONFIGURATION_LT => 'exclusiveMaximum',
                static::CONFIGURATION_LTE => 'maximum',
                static::CONFIGURATION_MAXIMUM_EXCLUSIVE => 'exclusiveMaximum',
                static::CONFIGURATION_MAXIMUM => 'maximum',
            ];

            foreach ($properties as $configurationName => $method) {
                if ($configuration->containsKey($configurationName)) {
                    $value = $configuration->get($configurationName);
                    $value = $isInteger ? intval($value) : floatval($value);
                    $property->$method($value);
                }
            }

            if ($configuration->containsKey(static::CONFIGURATION_RANGE)) {
                $range = $configuration->get(static::CONFIGURATION_RANGE);
                if (is_array($range) && 2 === count($range)) {
                    $range = array_values($range);
                    $property->range(
                        $isInteger ? intval($range[0]) : floatval($range[0]),
                        $isInteger ? intval($range[1]) : floatval($range[1])
                    );
                }
            }
        }

        return $property;
    }

    /**
     * @param ArraySchema $schema
     * @param AdvancedArrayObject $configuration
     *
     * @return ArraySchema
     *
     * @throws Exception
     */
    protected static function applyArray(ArraySchema $schema, AdvancedArrayObject $configuration): ArraySchema
    {
        self::applyArrayItems($schema, $configuration);
        self::applySchemaDefault($schema, $configuration);

        return $schema;
    }

    /**
     * @param ArraySchema|AssociativeArraySchema $schema
     * @param AdvancedArrayObject $configuration
     *
     * @return ArraySchema|AssociativeArraySchema
     * @throws Exception
     */
    protected static function applyArrayItems($schema, AdvancedArrayObject $configuration)
    {
        if ($schema instanceof ArraySchema || $schema instanceof AssociativeArraySchema) {
            if ($minItems = $configuration->getIntValue(static::CONFIGURATION_ITEMS_MIN)) {
                $schema->minItems($minItems);
            }

            if ($maxItems = $configuration->getIntValue(static::CONFIGURATION_ITEMS_MAX)) {
                $schema->maxItems($maxItems);
            }

            if ($configuration->containsKey(static::CONFIGURATION_ITEMS_UNIQUE)) {
                $schema->uniqueItems($configuration->getBooleanValue(static::CONFIGURATION_ITEMS_UNIQUE));
            }
        }

        return $schema;
    }

    /**
     * @param AssociativeSchema $schema
     * @param AdvancedArrayObject $configuration
     *
     * @return AssociativeSchema
     *
     * @throws Exception
     */
    protected static function applyObject(
        AssociativeSchema $schema,
        AdvancedArrayObject $configuration
    ): AssociativeSchema {
        if ($schema instanceof ObjectSchema) {
            if ($minProperties = $configuration->getIntValue(static::CONFIGURATION_PROPERTIES_MIN)) {
                $schema->minProperties($minProperties);
            }

            if ($maxProperties = $configuration->getIntValue(static::CONFIGURATION_PROPERTIES_MAX)) {
                $schema->maxProperties($maxProperties);
            }
        }

        if ($schema instanceof AssociativeArraySchema) {
            self::applyArrayItems($schema, $configuration);
        }

        self::applySchemaDefault($schema, $configuration);
        self::importProperties($schema, $configuration->get(static::PROPERTY_PROPERTIES, []));

        return $schema;
    }

    /**
     * @param Schema|ArraySchema|AssociativeSchema $schema
     * @param array $properties
     *
     * @return Schema|ArraySchema|AssociativeSchema|JsonSchema
     *
     * @throws Exception
     */
    protected static function importProperties(Schema $schema, array $properties)
    {
        foreach ($properties as $indexName => $configuration) {
            if (array_key_exists(static::CONFIGURATION_REFERENCE, $configuration)) {
                $schema->addByReference($indexName, strval($configuration[static::CONFIGURATION_REFERENCE]));
                continue;
            }

            if (array_key_exists(static::CONFIGURATION_REF, $configuration)) {
                $schema->addByReference($indexName, strval($configuration[static::CONFIGURATION_REF]));
                continue;
            }

            $schema->addProperty(self::createProperty($schema, $configuration, $indexName));
        }

        return $schema;
    }

    /**
     * @param Schema $parent
     * @param $configuration
     *
     * @return AbstractSchema|string
     *
     * @throws Exception
     */
    protected static function getConstraintOrString(Schema $parent, $configuration)
    {
        if (is_array($configuration)) {
            return self::createProperty($parent, $configuration);
        }

        if (!is_string($configuration)) {
            throw new RuntimeException('Invalid constraint configuration.');
        }

        if ($parent->getConstraintsFactory()->isConstraintTypeExists($configuration)) {
            $parent->getConstraintsFactory()->createConstraint($configuration, $parent);
        }

        return $configuration;
    }
}
