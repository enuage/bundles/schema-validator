<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Factory;

use Enuage\SchemaValidator\Constraint\AbstractSchema;
use Enuage\SchemaValidator\Constraint\ArraySchema;
use Enuage\SchemaValidator\Constraint\AssociativeArraySchema;
use Enuage\SchemaValidator\Constraint\BooleanSchema;
use Enuage\SchemaValidator\Constraint\FloatSchema;
use Enuage\SchemaValidator\Constraint\IntegerSchema;
use Enuage\SchemaValidator\Constraint\NumberSchema;
use Enuage\SchemaValidator\Constraint\ObjectSchema;
use Enuage\SchemaValidator\Constraint\ScalarSchema;
use Enuage\SchemaValidator\Constraint\Schema;
use Enuage\SchemaValidator\Constraint\SchemaInterface;
use Enuage\SchemaValidator\Constraint\SchemaPrototype;
use Enuage\SchemaValidator\Constraint\StringSchema;
use Enuage\Type\Helper\Type;
use Enuage\Type\PseudoGeneric;
use Exception;
use RuntimeException;

class ConstraintsFactory
{
    /**
     * @var PseudoGeneric<string, string>
     */
    protected $constraints;

    /**
     * ConstraintsFactory constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $this->setConstraints(
            [
                SchemaPrototype::getName() => SchemaPrototype::class,
                Schema::getName() => Schema::class,
                ArraySchema::getName() => ArraySchema::class,
                AssociativeArraySchema::getName() => AssociativeArraySchema::class,
                ObjectSchema::getName() => ObjectSchema::class,
                ScalarSchema::getName() => ScalarSchema::class,
                BooleanSchema::getName() => BooleanSchema::class,
                StringSchema::getName() => StringSchema::class,
                NumberSchema::getName() => NumberSchema::class,
                FloatSchema::getName() => FloatSchema::class,
                IntegerSchema::getName() => IntegerSchema::class,
            ]
        );
    }

    /**
     * @param array $constraints
     *
     * @return self
     *
     * @throws Exception
     */
    public function setConstraints(array $constraints): self
    {
        foreach ($constraints as $type => $className) {
            $this->setConstraint($type, $className);
        }

        return $this;
    }

    /**
     * @param string $type
     * @param string $className
     *
     * @return self
     *
     * @throws Exception
     */
    public function setConstraint(string $type, string $className): self
    {
        if (null === $this->constraints) {
            $this->constraints = new PseudoGeneric(Type::STRING_TYPE, Type::STRING_TYPE);
        }

        $this->constraints->set($type, $className);

        return $this;
    }

    /**
     * @param string $type
     * @param SchemaInterface $parent
     *
     * @return AbstractSchema
     *
     * @throws Exception
     * @throws RuntimeException
     */
    public function createConstraint(string $type, SchemaInterface $parent): AbstractSchema
    {
        if (null === $this->constraints) {
            throw new RuntimeException('No constraints are defined.');
        }

        /** @var AbstractSchema $constraint */
        $constraintClass = $this->constraints->get($type);

        if (null === $constraintClass) {
            throw new RuntimeException('Constraint with type "'.$type.'" could not be found.');
        }

        if (false === class_exists($constraintClass)) {
            throw new RuntimeException('Class "'.$constraintClass.'" does not exists.');
        }

        return new $constraintClass(null, $parent);
    }

    /**
     * @param string $type
     *
     * @return bool
     *
     * @throws Exception
     */
    public function isConstraintTypeExists(string $type): bool
    {
        return $this->constraints->containsKey($type);
    }
}
