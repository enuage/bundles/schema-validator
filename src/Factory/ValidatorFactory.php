<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Factory;

use ArrayIterator;
use Enuage\SchemaValidator\Constraint\ArraySchema;
use Enuage\SchemaValidator\Constraint\AssociativeArraySchema;
use Enuage\SchemaValidator\Constraint\ObjectSchema;
use Enuage\SchemaValidator\Validator\ArrayObjectValidator;
use Enuage\SchemaValidator\Validator\EmptyValidator;
use Enuage\SchemaValidator\Validator\JsonSchemaValidator;
use Enuage\SchemaValidator\Validator\SchemaValidator;
use Enuage\SchemaValidator\Validator\ValidatorInterface;
use Enuage\Type\Helper\Type;
use Enuage\Type\PseudoGeneric;
use Exception;

class ValidatorFactory
{
    /**
     * @var PseudoGeneric<string, string>
     */
    protected $validators;

    /**
     * ValidatorFactory constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $this->validators = new PseudoGeneric(
            Type::STRING_TYPE,
            Type::STRING_TYPE,
            [
                SchemaValidator::getName() => SchemaValidator::class,
                ArraySchema::getName() => ArrayObjectValidator::class,
                AssociativeArraySchema::getName() => ArrayObjectValidator::class,
                ObjectSchema::getName() => ArrayObjectValidator::class,
                JsonSchemaValidator::getName() => JsonSchemaValidator::class,
                EmptyValidator::getName() => EmptyValidator::class,
            ]
        );
    }

    /**
     * @param string $type
     * @param SchemaValidator|null $parent
     *
     * @return ValidatorInterface|null
     *
     * @throws Exception
     */
    public function getValidator(string $type, SchemaValidator $parent = null)
    {
        if (false === $this->validators->containsKey($type)) {
            return null;
        }

        $validatorClass = $this->validators->getStringValue($type);
        $validator = new $validatorClass;
        if ($validator instanceof SchemaValidator) {
            $validator->setValidatorFactory($parent ? $parent->getValidatorFactory() : $this);
        }

        return $validator;
    }

    /**
     * @param string $type
     * @param string $validatorClass
     *
     * @return self
     *
     * @throws Exception
     */
    public function setValidator(string $type, string $validatorClass): self
    {
        $this->validators->set($type, $validatorClass);

        return $this;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return $this->validators->getIterator();
    }
}
