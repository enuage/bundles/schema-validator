<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Factory;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatDateIsoConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatDateIsoOrdinalConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatDateTimeConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatEmailConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatIpAddressConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatPatternConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatTimeConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatTimestampConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatUriConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatUuidConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Format\FormatWeekIsoConstraint;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;
use InvalidArgumentException;

use function is_string;
use function reset;

class FormatConstraintFactory
{
    const DEFAULT_CONSTRAINTS = [
        AbstractFormatConstraint::FORMAT_DATE_ISO => FormatDateIsoConstraint::class,
        AbstractFormatConstraint::FORMAT_DATE_ISO_ORDINAL => FormatDateIsoOrdinalConstraint::class,
        AbstractFormatConstraint::FORMAT_DATETIME => FormatDateTimeConstraint::class,
        AbstractFormatConstraint::FORMAT_DATETIME_ISO => [FormatDateTimeConstraint::class, true],
        AbstractFormatConstraint::FORMAT_EMAIL => FormatEmailConstraint::class,
        AbstractFormatConstraint::FORMAT_IP => FormatIpAddressConstraint::class,
        AbstractFormatConstraint::FORMAT_IP_V4 => [FormatIpAddressConstraint::class, 4],
        AbstractFormatConstraint::FORMAT_IP_V6 => [FormatIpAddressConstraint::class, 6],
        AbstractFormatConstraint::FORMAT_PATTERN => FormatPatternConstraint::class,
        AbstractFormatConstraint::FORMAT_TIME => FormatTimeConstraint::class,
        AbstractFormatConstraint::FORMAT_TIME_12 => [FormatTimeConstraint::class, 12, false],
        AbstractFormatConstraint::FORMAT_TIME_12_SECONDS => [FormatTimeConstraint::class, 12, true],
        AbstractFormatConstraint::FORMAT_TIME_24 => [FormatTimeConstraint::class, 24, false],
        AbstractFormatConstraint::FORMAT_TIME_24_SECONDS => [FormatTimeConstraint::class, 24, true],
        AbstractFormatConstraint::FORMAT_TIMESTAMP => FormatTimestampConstraint::class,
        AbstractFormatConstraint::FORMAT_URI => FormatUriConstraint::class,
        AbstractFormatConstraint::FORMAT_UUID => FormatUuidConstraint::class,
        AbstractFormatConstraint::FORMAT_UUID_V1 => [FormatUuidConstraint::class, 1],
        AbstractFormatConstraint::FORMAT_UUID_V2 => [FormatUuidConstraint::class, 2],
        AbstractFormatConstraint::FORMAT_UUID_V3 => [FormatUuidConstraint::class, 3],
        AbstractFormatConstraint::FORMAT_UUID_V4 => [FormatUuidConstraint::class, 4],
        AbstractFormatConstraint::FORMAT_UUID_V5 => [FormatUuidConstraint::class, 5],
        AbstractFormatConstraint::FORMAT_WEEK_ISO => [FormatWeekIsoConstraint::class, false],
        AbstractFormatConstraint::FORMAT_WEEK_ISO_WEEKDAY => [FormatWeekIsoConstraint::class, true],
    ];

    /**
     * @var AdvancedArrayObject<string, string|array>
     */
    protected $constraints;

    /**
     * FormatConstraintFactory constructor.
     */
    public function __construct()
    {
        foreach (static::DEFAULT_CONSTRAINTS as $formatName => $options) {
            $this->registerConstraint($formatName, $options);
        }
    }

    /**
     * @param string $formatName
     * @param string|array $options
     *
     * @return self
     */
    public function registerConstraint(string $formatName, $options): self
    {
        if (false === TypeValidator::isValidTypes([Type::STRING_TYPE, Type::ARRAY_TYPE], $options)) {
            throw new InvalidArgumentException('Parameter "options" should be of type "string" or "array".');
        }

        if (null === $this->constraints) {
            $this->constraints = new AdvancedArrayObject();
        }

        $this->constraints->set($formatName, $options);

        return $this;
    }

    /**
     * @param string $formatName
     * @param array $options
     *
     * @return AbstractFormatConstraint
     *
     * @throws Exception
     */
    public function createConstraint(string $formatName, array $options = []): AbstractFormatConstraint
    {
        $constraintNotFoundException = new InvalidArgumentException('No constraint found with name "'.$formatName.'".');
        if (false === $this->constraints->containsKey($formatName)) {
            throw $constraintNotFoundException;
        }

        $constraintOptions = $this->constraints->get($formatName);
        if (is_array($constraintOptions)) {
            $className = reset($constraintOptions);
            unset($constraintOptions[0]);
        }

        if (is_string($constraintOptions)) {
            $className = $constraintOptions;
            $constraintOptions = [];
        }

        if (!isset($className)) {
            throw $constraintNotFoundException;
        }

        if (empty($constraintOptions)) {
            $constraintOptions = $options;
        }

        return new $className(...$constraintOptions);
    }
}
