<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator;

use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Chain;
use Exception;
use function gettype;

class ValidatableProperty
{
    /**
     * @var string
     */
    protected $indexName;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var ValidatableProperty|null
     */
    protected $parent = null;

    /**
     * ValidatableProperty constructor.
     *
     * @param string $indexName
     * @param mixed $value
     * @param ValidatableProperty|null $parent
     */
    public function __construct(string $indexName, $value, ValidatableProperty $parent = null)
    {
        if (is_object($value)) {
            if ($value instanceof AdvancedArrayObject) {
                $value = $value->getArrayCopy();
            }

            if ($value instanceof ValidatableProperty) {
                $value = $value->getValue();
            }
        }

        $this->indexName = $indexName;
        $this->value = $value;
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return gettype($this->getValue());
    }

    /**
     * @return Chain
     *
     * @throws Exception
     */
    public function getPointerChain(): Chain
    {
        $pointerChain = new Chain();
        $pointerChain->addNode($this->getIndexName());

        if ($parent = $this->getParent()) {
            $pointerChain->addNode($parent->getIndexName());
            while ($parent = $parent->getParent()) {
                $pointerChain->addNode($parent->getIndexName());
            }
        }

        return $pointerChain;
    }

    /**
     * @return string
     */
    public function getIndexName(): string
    {
        return $this->indexName;
    }

    /**
     * @return ValidatableProperty|null
     */
    public function getParent()
    {
        return $this->parent;
    }
}
