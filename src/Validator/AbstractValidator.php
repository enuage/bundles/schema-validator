<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Validator;

use Enuage\SchemaValidator\Constraint\AbstractSchema;
use Enuage\SchemaValidator\Constraint\PropertySchemaInterface;
use Enuage\SchemaValidator\Constraint\Validation\TypeConstraint;
use Enuage\SchemaValidator\Factory\ValidatorFactory;
use Enuage\SchemaValidator\Throwable\ConstraintValidationException;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Helper\Type;
use Exception;
use Ramsey\Uuid\Uuid;

abstract class AbstractValidator implements ValidatorInterface
{
    /**
     * @var ValidatorFactory
     */
    protected $validatorFactory;

    /**
     * @var AdvancedArrayObject
     */
    protected $violations;

    /**
     * @var AbstractSchema|null
     */
    protected $schema;

    /**
     * SchemaValidator constructor.
     *
     * @param ValidatorFactory|null $validatorFactory
     *
     * @throws Exception
     */
    public function __construct(ValidatorFactory $validatorFactory = null)
    {
        $this->setValidatorFactory($validatorFactory ?? new ValidatorFactory());
        $this->violations = new AdvancedArrayObject();
    }

    /**
     * @return ValidatorFactory
     */
    public function getValidatorFactory(): ValidatorFactory
    {
        return $this->validatorFactory;
    }

    /**
     * @param ValidatorFactory $validatorFactory
     *
     * @return self
     */
    public function setValidatorFactory(ValidatorFactory $validatorFactory): self
    {
        $this->validatorFactory = $validatorFactory;

        return $this;
    }

    /**
     * @param AbstractSchema $schema
     *
     * @return self
     */
    public function setSchema(AbstractSchema $schema): self
    {
        $this->schema = $schema;

        return $this;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->getViolations()->isEmpty();
    }

    /**
     * @return AdvancedArrayObject
     */
    public function getViolations(): AdvancedArrayObject
    {
        return $this->violations;
    }

    /**
     * @param $property
     * @param AbstractSchema $constraint
     *
     * @throws Exception
     * @throws ConstraintValidationException
     */
    protected function validateTypes(ValidatableProperty $property, AbstractSchema $constraint)
    {
        /** @var TypeConstraint $typeConstraint */
        $typeConstraint = $constraint->getValidationConstraints()->get(
            TypeConstraint::getName(),
            new TypeConstraint([Type::MIXED_TYPE])
        );

        if (false === $typeConstraint->isValid($property)) {
            throw new ConstraintValidationException($property, $typeConstraint);
        }
    }

    /**
     * @param mixed $data
     * @param AbstractSchema $schema
     * @param string|null $propertyName
     * @param ValidatableProperty|null $parent
     *
     * @return ValidatableProperty
     */
    protected function createProperty(
        $data,
        AbstractSchema $schema = null,
        string $propertyName = null,
        ValidatableProperty $parent = null
    ): ValidatableProperty {
        if ($data instanceof ValidatableProperty) {
            return $data;
        }

        if (null === $propertyName) {
            $propertyName = Uuid::uuid4()->toString();
            if (null !== $schema && $schema instanceof PropertySchemaInterface) {
                $propertyName = $schema->getPropertyName();
            }
        }

        return new ValidatableProperty($propertyName, $data, $parent);
    }
}
