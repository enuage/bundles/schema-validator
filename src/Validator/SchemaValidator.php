<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Validator;

use Enuage\SchemaValidator\Constraint\AbstractSchema;
use Enuage\SchemaValidator\Constraint\ArraySchema;
use Enuage\SchemaValidator\Constraint\AssociativeSchema;
use Enuage\SchemaValidator\Constraint\PropertySchema;
use Enuage\SchemaValidator\Constraint\Schema;
use Enuage\SchemaValidator\Constraint\Validation\AbstractCountConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\Throwable\ConstraintValidationException;
use Enuage\SchemaValidator\Throwable\SchemaValidationException;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;
use InvalidArgumentException;
use function array_key_exists;
use function array_keys;
use function get_object_vars;
use function in_array;
use function is_integer;
use function is_object;
use function sprintf;

class SchemaValidator extends AbstractValidator implements ValidatorInterface
{
    /**
     * @inheritDoc
     */
    public static function getName(): string
    {
        return Schema::getName();
    }

    /**
     * @param mixed $data
     * @param AbstractSchema $schema
     *
     * @return ValidatorInterface
     *
     * @throws ConstraintValidationException
     */
    public static function validateData($data, AbstractSchema $schema): ValidatorInterface
    {
        return (new self())->validate($data, $schema);
    }

    /** @inheritDoc */
    public function validate(
        $data,
        AbstractSchema $schema = null,
        ValidatorInterface $parent = null
    ): ValidatorInterface {
        /** @var AbstractSchema|Schema $configuration */
        $configuration = $schema ?? $this->schema;
        if (null === $configuration) {
            throw new InvalidArgumentException('No schema found for validation.');
        }

        $root = $parent ?? $this;
        try {
            $property = parent::createProperty($data, $schema);

            if (0 !== strcmp(Schema::getName(), $configuration::getName())) {
                return $this->validateConstraint($property, $configuration, $root);
            }

            $this->checkSchemaType($data);

            $this->validateItemsSchema($property, $configuration, $root);
            $this->validateItems($property, $configuration, $root);

            $this->validateProperties($property, $configuration);
        } catch (SchemaValidationException $exception) {
            $root->getViolations()->push($exception);
        }

        return $root;
    }

    /**
     * @param ValidatableProperty $property
     * @param AbstractSchema $schema
     * @param AbstractValidator $root
     *
     * @return ValidatorInterface
     *
     * @throws Exception
     * @throws ConstraintValidationException
     */
    protected function validateConstraint(
        ValidatableProperty $property,
        AbstractSchema $schema,
        AbstractValidator $root
    ): ValidatorInterface {
        $this->validateTypes($property, $schema);

        if (false === $this->isValid()) {
            return $this;
        }

        /** @var ValidatorInterface|AbstractValidator $validator */
        $validator = $this->getValidatorFactory()->getValidator($schema::getName());
        if (null === $validator) {
            $this->runDefinedValidations($property, $schema);

            return $this;
        }

        $validator->setValidatorFactory($this->getValidatorFactory());

        return $validator->validate($property, $schema, $root);
    }

    /**
     * @param ValidatableProperty $property
     * @param AbstractSchema $schema
     */
    protected function runDefinedValidations(ValidatableProperty $property, AbstractSchema $schema)
    {
        $schema->getValidationConstraints()->map(
            function (ValidationConstraint $constraint) use ($property) {
                if (false === $constraint->isValid($property)) {
                    throw new ConstraintValidationException($property, $constraint);
                }
            }
        );
    }

    /**
     * @param $data
     *
     * @throws InvalidArgumentException
     */
    protected function checkSchemaType($data)
    {
        if (false === TypeValidator::isValid(Type::ARRAY_OBJECT_TYPE, $data)) {
            throw new InvalidArgumentException('Unable to validate data: unsupported data type.');
        }
    }

    /**
     * @param ValidatableProperty $property
     * @param Schema $schema
     * @param AbstractValidator $root
     *
     * @throws ConstraintValidationException
     */
    protected function validateItemsSchema(ValidatableProperty $property, Schema $schema, AbstractValidator $root)
    {
        $itemsSchema = $schema->getItemsSchema();
        if (null === $itemsSchema) {
            return;
        }

        $countValidators = $schema->getValidationConstraints()->filter(
            function (ValidationConstraint $constraint) {
                return $constraint instanceof AbstractCountConstraint;
            }
        );

        if ($countValidators->count() > 0) {
            $this->validateCount($property, $schema);

            if (false === $this->isValid()) {
                return;
            }
        }

        foreach ($property->getValue() as $index => $item) {
            $this->validateConstraint(
                parent::createProperty($item, null, strval($index), $property),
                $itemsSchema,
                $root
            );

            if (false === $this->isValid()) {
                return;
            }
        }
    }

    /**
     * @param ValidatableProperty $property
     * @param Schema $schemaConstraint
     */
    protected function validateCount(ValidatableProperty $property, Schema $schemaConstraint)
    {
        $schemaConstraint->getValidationConstraints()->filter(function (ValidationConstraint $validationConstraint) {
            return $validationConstraint instanceof AbstractCountConstraint;
        })->map(function (ValidationConstraint $constraint) use ($property) {
            if (false === $constraint->isValid($property)) {
                throw new ConstraintValidationException($property, $constraint);
            }
        });
    }

    /**
     * @param ValidatableProperty $property
     * @param Schema $schema
     * @param AbstractValidator $root
     *
     * @throws Exception
     * @throws ConstraintValidationException
     * @throws SchemaValidationException
     */
    protected function validateItems(ValidatableProperty $property, Schema $schema, AbstractValidator $root)
    {
        $properties = $property->getValue();

        $this->checkSchemaType($properties);

        /** @var AdvancedArrayObject|null $schemaItems */
        $schemaItems = $schema->getItems();
        if (null === $schemaItems) {
            return;
        }

        foreach ($properties as $index => $item) {
            $schemaItem = $schemaItems->get($schema instanceof AssociativeSchema ? $index : strval($item));
            if (null === $schemaItem) {
                throw new SchemaValidationException('Schema contains unsupported item.');
            }

            if ($schemaItem instanceof AbstractSchema) {
                $this->validateConstraint(
                    parent::createProperty($item, null, strval($index), $property),
                    $schemaItem,
                    $root
                );
            }
        }
    }

    /**
     * @param ValidatableProperty $property
     * @param Schema $schema
     *
     * @return self
     *
     * @throws ConstraintValidationException
     * @throws Exception
     * @throws SchemaValidationException
     */
    protected function validateProperties(ValidatableProperty $property, Schema $schema): self
    {
        $properties = $property->getValue();

        $this->getValidatorFactory()->getValidator(EmptyValidator::getName(), $this)->validate($property, $schema);

        /** @var Schema $configuration */
        $this->validateCount($property, $schema);
        $this->validateRequiredItems($property, $schema);

        if ($schema instanceof AssociativeSchema) {
            $keys = is_object($properties) ? array_keys(get_object_vars($properties)) : array_keys($properties);
            foreach ($keys as $index) {
                if (is_integer($index)) {
                    continue;
                }

                if (
                    false === $schema->getProperties()->containsKey($index) &&
                    false === ($schema->getItems() ?? new AdvancedArrayObject())->containsKey($index) &&
                    false === ($schema->getRequiredItems() ?? new AdvancedArrayObject())->containsElement($index)
                ) {
                    if ($schema->isAdditionalPropertiesAllowed()) {
                        $additionalProperties = $schema->getAdditionalProperties();
                        if (null === $additionalProperties) {
                            continue;
                        }

                        if (true === $additionalProperties->containsKey($index)) {
                            continue;
                        }
                    }

                    throw new SchemaValidationException(
                        sprintf('Property "%s" is not allowed as child of "%s".', $index, $property->getIndexName())
                    );
                }
            }
        }

        $this->runDefinedValidations($property, $schema);

        return $this;
    }

    /**
     * @param ValidatableProperty $property
     * @param Schema $schema
     *
     * @throws Exception
     * @throws SchemaValidationException
     */
    protected function validateRequiredItems(ValidatableProperty $property, Schema $schema)
    {
        $requiredItems = $schema->getRequiredItems() ?? new AdvancedArrayObject();

        if ($schema instanceof AssociativeSchema) {
            $schema->getProperties()->filter(function (AbstractSchema $propertySchema) {
                return $propertySchema->isRequired();
            })->map(function (PropertySchema $propertySchema) use ($requiredItems) {
                $propertyName = $propertySchema->getPropertyName();
                if (false === $requiredItems->containsElement($propertyName)) {
                    $requiredItems->push($propertyName);
                }
            });
        }

        $propertyValue = $property->getValue();
        foreach ($requiredItems->getIterator() as $requiredItem) {
            $exists = array_key_exists($requiredItem, $propertyValue);
            if ($schema instanceof ArraySchema) {
                $exists = in_array($requiredItem, $propertyValue);
            }

            if (false === $exists) {
                throw new SchemaValidationException(
                    sprintf(
                        'The following items are required for "{propertyName}": ["%s"].',
                        $requiredItems->implode('","')
                    )
                );
            }
        }
    }
}
