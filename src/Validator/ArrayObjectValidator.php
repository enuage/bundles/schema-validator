<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Validator;

use Enuage\SchemaValidator\Constraint\AbstractSchema;
use Enuage\SchemaValidator\Constraint\ArraySchema;
use Enuage\SchemaValidator\Constraint\AssociativeSchema;
use Enuage\SchemaValidator\Constraint\ObjectSchema;
use Enuage\SchemaValidator\ValidatableProperty;
use function array_key_exists;
use function get_object_vars;
use function is_array;
use function is_object;

class ArrayObjectValidator extends SchemaValidator implements ValidatorInterface
{
    /**
     * @inheritDoc
     *
     * @param ArraySchema|ObjectSchema $schema
     */
    public function validate(
        $data,
        AbstractSchema $schema = null,
        ValidatorInterface $parent = null
    ): ValidatorInterface {
        parent::checkSchemaType($data);

        $property = parent::createProperty($data, $schema);

        $root = $parent ?? $this;
        parent::validateItemsSchema($property, $schema, $root);
        parent::validateItems($property, $schema, $root);

        parent::validateProperties($property, $schema);

        if ($schema instanceof AssociativeSchema) {
            /**
             * @var string $propertyName
             * @var AbstractSchema $propertySchema
             */
            foreach ($schema->getProperties() as $propertyName => $propertySchema) {
                $propertyValue = null;

                $value = $property->getValue();
                if (is_object($value)) {
                    $objectVars = get_object_vars($value);
                    if (array_key_exists($propertyName, $objectVars)) {
                        $propertyValue = $objectVars[$propertyName];
                    }
                }

                if (is_array($value)) {
                    if (array_key_exists($propertyName, $value)) {
                        $propertyValue = $value[$propertyName];
                    }
                }

                if (null === $propertyValue) {
                    continue;
                }

                parent::validate(
                    new ValidatableProperty($propertyName, $propertyValue, $property),
                    $propertySchema,
                    $parent ?? $this
                );
            }
        }

        return $root;
    }
}
