<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Validator;

use Enuage\SchemaValidator\Constraint\AbstractSchema;
use Enuage\SchemaValidator\Constraint\JsonSchema;
use Enuage\SchemaValidator\Constraint\Validation\JsonConstraint;
use Enuage\SchemaValidator\Throwable\ConstraintValidationException;
use Enuage\SchemaValidator\ValidatableProperty;

class JsonSchemaValidator extends SchemaValidator
{
    /** @inheritDoc */
    public static function getName(): string
    {
        return JsonSchema::getName();
    }

    /**
     * @inheritDoc
     *
     * @param JsonSchema|null $schema
     */
    public function validate(
        $data,
        AbstractSchema $schema = null,
        ValidatorInterface $parent = null
    ): ValidatorInterface {
        /** @var JsonConstraint $jsonConstraint */
        $jsonConstraint = $schema->getValidationConstraints()->get(JsonConstraint::getName(), new JsonConstraint());
        if (false === $jsonConstraint->isValid($data)) {
            throw new ConstraintValidationException($data, $jsonConstraint);
        }

        if ($data instanceof ValidatableProperty) {
            $data = $data->getValue();
        }

        return parent::validateProperties(parent::createProperty(json_decode($data), $schema), $schema);
    }
}
