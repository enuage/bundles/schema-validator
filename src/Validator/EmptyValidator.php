<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Validator;

use Enuage\SchemaValidator\Constraint\AbstractSchema;
use Enuage\SchemaValidator\Constraint\Validation\NotEmptyConstraint;
use Enuage\SchemaValidator\Throwable\ConstraintValidationException;

class EmptyValidator extends AbstractValidator implements ValidatorInterface
{
    /** @inheritDoc */
    public static function getName(): string
    {
        return NotEmptyConstraint::getName();
    }

    /** @inheritDoc */
    public function validate($data, AbstractSchema $schema, ValidatorInterface $parent = null): ValidatorInterface
    {
        /** @var NotEmptyConstraint|null $emptyConstraint */
        $emptyConstraint = $schema->getValidationConstraints()->get(NotEmptyConstraint::getName());
        if (null === $emptyConstraint) {
            return $this;
        }

        if (false === $emptyConstraint->isValid($data)) {
            throw new ConstraintValidationException($data, $emptyConstraint);
        }

        return $parent ?? $this;
    }
}
