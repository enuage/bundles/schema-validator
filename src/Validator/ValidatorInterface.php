<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Validator;

use Enuage\SchemaValidator\Constraint\AbstractSchema;
use Enuage\SchemaValidator\Throwable\ConstraintValidationException;
use Enuage\Type\AdvancedArrayObject;
use Exception;

interface ValidatorInterface
{
    /**
     * @return string
     */
    public static function getName(): string;

    /**
     * @param mixed $data
     * @param AbstractSchema $schema
     * @param ValidatorInterface|null $parent
     *
     * @return ValidatorInterface
     *
     * @throws Exception
     * @throws ConstraintValidationException
     */
    public function validate(
        $data,
        AbstractSchema $schema,
        ValidatorInterface $parent = null
    ): ValidatorInterface;

    /**
     * @return bool
     */
    public function isValid(): bool;

    /**
     * @return AdvancedArrayObject
     */
    public function getViolations(): AdvancedArrayObject;
}
