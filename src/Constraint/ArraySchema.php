<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\SchemaValidator\Constraint\Validation\UniqueItemsConstraint;
use Enuage\Type\Helper\Type;
use Exception;

class ArraySchema extends Schema
{
    /**
     * @var SchemaInterface|null
     */
    protected $parent;

    /** @inheritDoc */
    public function __construct(string $title = null, SchemaInterface $parent = null)
    {
        parent::__construct($title, $parent);

        $this->setType(Type::ARRAY_TYPE);
        $this->parent = $parent;
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return Type::ARRAY_TYPE;
    }

    /**
     * @return AssociativeArraySchema
     *
     * @throws Exception
     */
    public function associative(): AssociativeArraySchema
    {
        $schema = new AssociativeArraySchema($this->propertyName, $this->parent);

        return $schema->cloneArraySchema($this);
    }

    /**
     * @param int $maxItems
     *
     * @return self
     *
     * @throws Exception
     */
    public function maxItems(int $maxItems): self
    {
        $this->setMaxCount($maxItems);

        return $this;
    }

    /**
     * @param int $minItems
     *
     * @return self
     *
     * @throws Exception
     */
    public function minItems(int $minItems): self
    {
        $this->setMinCount($minItems);

        return $this;
    }

    /**
     * @param bool $unique
     *
     * @return self
     *
     * @throws Exception
     */
    public function uniqueItems(bool $unique = true): self
    {
        if (true === $unique) {
            $this->setValidationConstraint(new UniqueItemsConstraint());
        }

        return $this;
    }
}
