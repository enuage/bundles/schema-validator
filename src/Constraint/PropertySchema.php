<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Ramsey\Uuid\Uuid;

class PropertySchema extends SchemaPrototype implements PropertySchemaInterface
{
    /**
     * @var string|null
     */
    protected $propertyName;

    /** @inheritDoc */
    public function getPropertyName(): string
    {
        return $this->propertyName ?? Uuid::uuid4()->toString();
    }

    /** @inheritDoc */
    public function setPropertyName(string $propertyName): self
    {
        $this->propertyName = $propertyName;

        return $this;
    }
}
