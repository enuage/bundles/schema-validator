<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\Type\Helper\Type;
use Exception;

class FloatSchema extends NumberSchema
{
    /** @inheritDoc */
    public function __construct(string $propertyName = null)
    {
        parent::__construct($propertyName);

        $this->setTypes([Type::INTEGER_TYPE, Type::FLOAT_TYPE]);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return Type::FLOAT_TYPE;
    }

    /**
     * @param float $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function neq(float $value): self
    {
        return $this->notEqualTo($value);
    }

    /**
     * @param float $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function notEqualTo(float $value): self
    {
        parent::setNotEqual($value);

        return $this;
    }

    /**
     * @param float $minimalValue
     *
     * @return self
     *
     * @throws Exception
     */
    public function gt(float $minimalValue): self
    {
        return $this->exclusiveMinimum($minimalValue);
    }

    /**
     * @param float $minimalValue
     *
     * @return FloatSchema
     *
     * @throws Exception
     */
    public function exclusiveMinimum(float $minimalValue): FloatSchema
    {
        return $this->minimum($minimalValue, true);
    }

    /**
     * @param float $minimalValue
     * @param bool $isExclusive
     *
     * @return FloatSchema
     *
     * @throws Exception
     */
    public function minimum(float $minimalValue, bool $isExclusive = false): FloatSchema
    {
        parent::setMinValue($minimalValue, $isExclusive);

        return $this;
    }

    /**
     * @param float $minimalValue
     *
     * @return FloatSchema
     *
     * @throws Exception
     */
    public function gte(float $minimalValue): FloatSchema
    {
        return $this->minimum($minimalValue);
    }

    /**
     * @param float $maximalValue
     *
     * @return FloatSchema
     *
     * @throws Exception
     */
    public function lt(float $maximalValue): FloatSchema
    {
        return $this->exclusiveMaximum($maximalValue);
    }

    /**
     * @param float $maximumValue
     *
     * @return FloatSchema
     *
     * @throws Exception
     */
    public function exclusiveMaximum(float $maximumValue): FloatSchema
    {
        return $this->maximum($maximumValue, true);
    }

    /**
     * @param float $maximalValue
     * @param bool $isExclusive
     *
     * @return FloatSchema
     *
     * @throws Exception
     */
    public function maximum(float $maximalValue, bool $isExclusive = false): FloatSchema
    {
        parent::setMaxValue($maximalValue, $isExclusive);

        return $this;
    }

    /**
     * @param float $maximalValue
     *
     * @return FloatSchema
     *
     * @throws Exception
     */
    public function lte(float $maximalValue): FloatSchema
    {
        return $this->maximum($maximalValue);
    }

    /**
     * @param float $minimalValue
     * @param float $maximalValue
     *
     * @return FloatSchema
     *
     * @throws Exception
     */
    public function range(float $minimalValue, float $maximalValue): FloatSchema
    {
        parent::setRange($minimalValue, $maximalValue);

        return $this;
    }
}
