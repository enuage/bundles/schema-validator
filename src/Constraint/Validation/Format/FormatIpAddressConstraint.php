<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;

class FormatIpAddressConstraint extends AbstractFormatConstraint implements ValidationConstraint
{
    /**
     * @var int
     */
    protected $version;

    /**
     * FormatIpAddressConstraint constructor.
     *
     * @param int $version
     */
    public function __construct(int $version = 4)
    {
        parent::__construct(static::getName());

        $this->version = $version;
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_IP;
    }

    /** @inheritDoc */
    public function isValid(ValidatableProperty $property): bool
    {
        switch ($this->version) {
            case 6:
                $ipFlag = FILTER_FLAG_IPV6;
                break;
            case 4:
            default:
                $ipFlag = FILTER_FLAG_IPV4;
                break;
        }

        return null !== filter_var($property->getValue(), FILTER_VALIDATE_IP, FILTER_NULL_ON_FAILURE | $ipFlag);
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return sprintf('The value of property "{propertyName}" has invalid IPv%d address format.', $this->version);
    }
}
