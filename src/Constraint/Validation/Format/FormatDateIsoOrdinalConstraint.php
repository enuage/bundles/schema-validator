<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Helper\NumberHelper;
use Exception;

use function intval;
use function preg_match;

class FormatDateIsoOrdinalConstraint extends AbstractFormatDateConstraint implements ValidationConstraint
{
    /**
     * FormatDateIsoOrdinalConstraint constructor.
     */
    public function __construct()
    {
        parent::__construct(static::getName(), 'Y-z');
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_DATE_ISO_ORDINAL;
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        $this->checkType($property);

        $dateMatch = preg_match('/\d{4}-(\d{1,3})/', $property->getValue(), $dateMatches);
        if (1 !== $dateMatch) {
            return false;
        }

        /** @noinspection PhpUnusedLocalVariableInspection */
        list($year, $dayOfYear) = $dateMatches;

        return NumberHelper::inRange(intval($dayOfYear), 1, 366);
    }
}
