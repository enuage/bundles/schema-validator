<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;

class FormatDateIsoConstraint extends AbstractFormatDateConstraint implements ValidationConstraint
{
    /**
     * FormatDateIsoConstraint constructor.
     */
    public function __construct()
    {
        parent::__construct(static::getName(), 'Y-m-d');
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_DATE_ISO;
    }
}
