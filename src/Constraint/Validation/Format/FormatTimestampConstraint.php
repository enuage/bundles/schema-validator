<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use DateTime;
use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Exception;

class FormatTimestampConstraint extends AbstractFormatConstraint implements ValidationConstraint
{
    /**
     * FormatTimestampConstraint constructor.
     */
    public function __construct()
    {
        parent::__construct(static::getName());
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_TIMESTAMP;
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        $this->checkType($property);

        try {
            new DateTime('@'.$property->getValue());

            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return 'The value of property "{propertyName}" has invalid UNIX timestamp format.';
    }
}
