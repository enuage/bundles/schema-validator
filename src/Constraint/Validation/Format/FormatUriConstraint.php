<?php


declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;


use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;

class FormatUriConstraint extends AbstractFormatConstraint implements ValidationConstraint
{
    /**
     * FormatUriConstraint constructor.
     */
    public function __construct()
    {
        parent::__construct(static::getName());
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_URI;
    }

    /**
     * @inheritDoc
     */
    public function isValid(ValidatableProperty $property): bool
    {
        return null !== filter_var($property->getValue(), FILTER_VALIDATE_URL, FILTER_NULL_ON_FAILURE);
    }

    /**
     * @inheritDoc
     */
    public function getErrorMessage(string $type = null): string
    {
        return 'The value of property "{propertyName}" has invalid URI format.';
    }
}
