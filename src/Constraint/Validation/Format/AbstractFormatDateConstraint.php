<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use DateTime;
use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Exception;

abstract class AbstractFormatDateConstraint extends AbstractFormatConstraint
{
    const MAX_WEEK_NUMBER = 52;
    const MAX_WEEKDAY_NUMBER = 7;

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return sprintf(
            'The value of property "{propertyName}" has invalid date/time format. Example of valid format: "%s"',
            $this->getPattern()
        );
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        $this->checkType($property);

        return $this->isDateTimeFormatValid($property);
    }

    /**
     * @param ValidatableProperty $property
     *
     * @return bool
     */
    protected function isDateTimeFormatValid(ValidatableProperty $property): bool
    {
        $dateTime = DateTime::createFromFormat($this->getPattern(), $property->getValue());
        if (false === $dateTime) {
            return false;
        }

        return $property->getValue() === $dateTime->format($this->getPattern());
    }
}
