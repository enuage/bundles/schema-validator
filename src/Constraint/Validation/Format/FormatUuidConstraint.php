<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Helper\NumberHelper;
use Exception;

use function sprintf;
use function strval;

class FormatUuidConstraint extends AbstractFormatConstraint implements ValidationConstraint
{
    const MIN_UUID_VERSION = 1;
    const MAX_UUID_VERSION = 5;
    const PATTERN = '/^[0-9A-F]{8}-[0-9A-F]{4}-[%s]{1}[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';

    /**
     * @var int|null
     */
    protected $version;

    /**
     * FormatUuidConstraint constructor.
     *
     * @param int|null $version
     */
    public function __construct(int $version = null)
    {
        parent::__construct(static::getName());

        $this->version = $version;
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_UUID;
    }

    /** @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        return null !== filter_var(
                $property->getValue(),
                FILTER_VALIDATE_REGEXP,
                [
                    'options' => [
                        'regexp' => sprintf(static::PATTERN, strval($this->getVersion() ?? '1-5')),
                    ],
                    'flags' => FILTER_NULL_ON_FAILURE,
                ]
            );
    }

    /**
     * @return int|null
     *
     * @throws Exception
     */
    protected function getVersion()
    {
        $version = $this->version;
        if (
            null !== $this->version &&
            NumberHelper::inRange($version, static::MIN_UUID_VERSION, static::MAX_UUID_VERSION)
        ) {
            return $version;
        }

        return null;
    }

    /** @inheritDoc
     *
     * @throws Exception
     */
    public function getErrorMessage(string $type = null): string
    {
        if ($version = $this->getVersion()) {
            $messageVersion = ' (Version '.strval($version).')';
        }

        return sprintf('The value of property "{propertyName}" has invalid UUID format%s.', $messageVersion ?? '');
    }
}
