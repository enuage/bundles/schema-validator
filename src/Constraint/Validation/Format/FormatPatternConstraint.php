<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Exception;

class FormatPatternConstraint extends AbstractFormatConstraint implements ValidationConstraint
{
    /**
     * FormatPatternConstraint constructor.
     *
     * @param string $pattern
     */
    public function __construct(string $pattern)
    {
        parent::__construct(static::getName(), $pattern);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_PATTERN;
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        $this->checkType($property);

        return null !== filter_var(
                $property->getValue(),
                FILTER_VALIDATE_REGEXP,
                [
                    'options' => [
                        'regexp' => $this->getPattern(),
                    ],
                    'flags' => FILTER_NULL_ON_FAILURE,
                ]
            );
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return 'Provided value for property "{propertyName}" does not matches required pattern.';
    }
}
