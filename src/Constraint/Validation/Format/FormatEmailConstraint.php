<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;

class FormatEmailConstraint extends AbstractFormatConstraint implements ValidationConstraint
{
    /**
     * FormatEmailConstraint constructor.
     */
    public function __construct()
    {
        parent::__construct(static::getName());
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_EMAIL;
    }

    /** @inheritDoc */
    public function isValid(ValidatableProperty $property): bool
    {
        return null !== filter_var($property->getValue(), FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE);
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return 'The value of property "{propertyName}" has invalid e-mail address format.';
    }
}
