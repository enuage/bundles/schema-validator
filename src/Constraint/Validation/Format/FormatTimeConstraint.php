<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;

use function intval;

class FormatTimeConstraint extends AbstractFormatDateConstraint implements ValidationConstraint
{
    const PATTERNS = [
        12 => ['G:i A', 'G:i:s A'],
        24 => ['H:i', 'H:i:s'],
    ];

    /**
     * FormatTime12Constraint constructor.
     *
     * @param int $format
     * @param bool $isSecondsEnabled
     */
    public function __construct(int $format = 24, bool $isSecondsEnabled = true)
    {
        if (12 !== $format && 24 !== $format) {
            $format = 24;
        }

        parent::__construct(static::getName(), static::PATTERNS[$format][intval($isSecondsEnabled)]);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_TIME;
    }
}
