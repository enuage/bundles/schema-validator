<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Helper\NumberHelper;
use Exception;

use function preg_match;

class FormatWeekIsoConstraint extends AbstractFormatDateConstraint implements ValidationConstraint
{
    const DATE_WEEK_ISO = 'Y-\WW';
    const DATE_WEEK_ISO_WEEK_DAY = 'Y-\WW-N';

    const PATTERN_WEEK = '/(\d{4})-[W]([0-9]{1,2})/';
    const PATTERN_WEEK_WEEK_DAY = '/(\d{4})-[W]([0-9]{1,2})-([1-7])/';

    /**
     * @var bool
     */
    protected $weekDayEnabled;

    /**
     * FormatWeekIsoConstraint constructor.
     *
     * @param bool $isWeekDayEnabled
     */
    public function __construct(bool $isWeekDayEnabled = false)
    {
        $this->weekDayEnabled = $isWeekDayEnabled;

        parent::__construct(
            static::getName(),
            $isWeekDayEnabled ? static::DATE_WEEK_ISO_WEEK_DAY : static::DATE_WEEK_ISO
        );
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_WEEK_ISO;
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        $this->checkType($property);

        $pattern = $this->weekDayEnabled ? static::PATTERN_WEEK_WEEK_DAY : static::PATTERN_WEEK;
        $dateMatch = preg_match($pattern, $property->getValue(), $dateMatches);
        if (1 !== $dateMatch) {
            return false;
        }

        /** @noinspection PhpUnusedLocalVariableInspection */
        list($fullMatch, $year, $weekNumber) = $dateMatches;

        return NumberHelper::inRange(intval($weekNumber), 1, static::MAX_WEEK_NUMBER);
    }
}
