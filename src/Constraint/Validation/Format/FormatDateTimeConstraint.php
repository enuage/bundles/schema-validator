<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Format;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;

class FormatDateTimeConstraint extends AbstractFormatDateConstraint implements ValidationConstraint
{
    const PATTERN = 'Y-m-d H:i:s';
    const PATTERN_ISO = 'Y-m-d\TH:i:sP';

    /**
     * @var bool|false
     */
    protected $isIso = false;

    /**
     * FormatDateTimeConstraint constructor.
     *
     * @param bool $isIso
     */
    public function __construct(bool $isIso = false)
    {
        parent::__construct(static::getName(), $isIso ? static::PATTERN_ISO : static::PATTERN);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractFormatConstraint::FORMAT_DATETIME;
    }
}
