<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation;

use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\PseudoEnum;
use Exception;

/**
 * Class EnumConstraint
 *
 * @package Enuage\SchemaValidator\Constraint\Validation
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class EnumConstraint implements ValidationConstraint
{
    const CONSTRAINT_NAME = 'enum';

    /**
     * @var PseudoEnum
     */
    protected $values;

    /**
     * EnumConstraint constructor.
     *
     * @param array $values
     *
     * @throws Exception
     */
    public function __construct(array $values)
    {
        $this->values = new PseudoEnum($values);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return static::CONSTRAINT_NAME;
    }

    /** @inheritDoc */
    public function isValid(ValidatableProperty $property): bool
    {
        return $this->values->containsValue($property->getValue());
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return sprintf(
            'Property "{propertyName}" contains unacceptable value ("{propertyValue}"). Allowed values for this property: ["%s"].',
            $this->values->getElements()->implode('", "')
        );
    }
}
