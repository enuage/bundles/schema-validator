<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation;

use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;

use function array_unique;
use function count;

class UniqueItemsConstraint implements ValidationConstraint
{
    /** @inheritDoc */
    public static function getName(): string
    {
        return 'uniqueItems';
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        $value = $property->getValue();
        TypeValidator::validate(Type::ARRAY_OBJECT_TYPE, $value);
        TypeValidator::validate(Type::COUNTABLE_TYPE, $value);

        if ($value instanceof AdvancedArrayObject) {
            return $value->count() === count(array_unique($value->getValues(), SORT_REGULAR));
        }

        return count($value) === count(array_unique($value, SORT_REGULAR));
    }

    /**
     * @inheritDoc
     */
    public function getErrorMessage(string $type = null): string
    {
        return 'Property "{propertyName}" should contain unique values.';
    }
}
