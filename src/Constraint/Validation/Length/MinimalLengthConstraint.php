<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Length;

use Enuage\SchemaValidator\Constraint\Validation\AbstractLengthConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Exception;

use function strlen;

class MinimalLengthConstraint extends AbstractLengthConstraint implements ValidationConstraint
{
    /**
     * MinimalLengthConstraint constructor.
     *
     * @param int $requiredValue
     */
    public function __construct(int $requiredValue)
    {
        parent::__construct(static::getName(), $requiredValue);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractLengthConstraint::LENGTH_MINIMUM;
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        $this->checkType($property);

        return $this->getValue() <= strlen($property->getValue());
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return 'The value length of property "{propertyName}" should be more than "{constraintValue}" symbols.';
    }
}
