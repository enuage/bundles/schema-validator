<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation;

use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;

use function abs;

class PositiveConstraint implements ValidationConstraint
{
    /**
     * @var bool
     */
    protected $isPositive;

    /**
     * PositiveConstraint constructor.
     *
     * @param bool $isPositive
     */
    public function __construct(bool $isPositive)
    {
        $this->isPositive = $isPositive;
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return 'positive';
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        $value = $property->getValue();
        TypeValidator::validate(Type::NUMBER_TYPE, $value);

        if (0 === abs($value) || 0.0 === abs($value)) {
            return true;
        }

        return $this->isPositive ? 0 < $value : 0 > $value;
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return 'The value of property "{propertyName}" should be '.($this->isPositive ? 'positive' : 'negative').'.';
    }
}
