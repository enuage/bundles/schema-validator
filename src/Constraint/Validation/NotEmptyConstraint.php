<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation;

use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;

use function count;
use function get_object_vars;
use function is_object;

class NotEmptyConstraint implements ValidationConstraint
{
    /**
     * @var bool
     */
    protected $emptyAllowed;

    /**
     * EmptyConstraint constructor.
     *
     * @param bool $isEmptyAllowed
     */
    public function __construct(bool $isEmptyAllowed = false)
    {
        $this->emptyAllowed = $isEmptyAllowed;
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return 'empty';
    }

    /** @inheritDoc */
    public function isValid(ValidatableProperty $property): bool
    {
        if (true === $this->emptyAllowed) {
            return true;
        }

        $value = $property->getValue();
        if (is_object($value)) {
            if (TypeValidator::isValid(Type::COUNTABLE_TYPE, $value)) {
                return 0 !== count($value);
            }

            return 0 !== count(get_object_vars($value));
        }

        return !empty($value);
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return 'The value of "{propertyName}" can not be empty.';
    }
}
