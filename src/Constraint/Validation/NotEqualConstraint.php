<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation;

use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Exception;

use function hash;
use function hash_equals;
//use function is_array;
//use function is_object;
//use function json_encode;
//use function spl_object_hash;
use function strval;

class NotEqualConstraint implements ValidationConstraint
{
    /**
     * @var mixed
     */
    protected $value;

    /**
     * NotEqualConstraint constructor.
     *
     * @param mixed $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return 'notEqual';
    }

    /**
     * @inheritDoc
     */
    public function isValid(ValidatableProperty $property): bool
    {
        return false === hash_equals($this->hash($this->value), $this->hash($property->getValue()));
    }

    /**
     * @param object|array|mixed $value
     *
     * @return string
     */
    protected function hash($value): string
    {
//        if (is_object($value)) {
//            return spl_object_hash($value);
//        }

//        if (is_array($value)) {
//            $value = json_encode($value);
//        }

        return hash('sha256', strval($value));
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function getErrorMessage(string $type = null): string
    {
        return 'The value of property "{propertyName}" is invalid.';
    }
}
