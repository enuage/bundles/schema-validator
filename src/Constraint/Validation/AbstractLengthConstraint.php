<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation;

use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;

abstract class AbstractLengthConstraint
{
    const LENGTH_MINIMUM = 'minLength';
    const LENGTH_MAXIMUM = 'maxLength';
    const LENGTH_REQUIRED = 'requiredLength';

    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     */
    protected $value;

    /**
     * AbstractLengthConstraint constructor.
     *
     * @param string $type
     * @param int $value
     */
    public function __construct(string $type, int $value)
    {
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param ValidatableProperty $data
     *
     * @throws Exception
     */
    protected function checkType(ValidatableProperty $data)
    {
        TypeValidator::validate(Type::STRING_TYPE, $data->getValue());
    }
}
