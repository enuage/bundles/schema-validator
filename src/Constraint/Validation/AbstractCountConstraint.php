<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation;

use Countable;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;

use function count;
use function get_object_vars;
use function is_object;

abstract class AbstractCountConstraint implements ValidationConstraint
{
    const COUNT_MINIMUM = 'countMinimum';
    const COUNT_MAXIMUM = 'countMaximum';
    const COUNT_MULTIPLICITY = 'countMultiplicity';
    const COUNT_EXACT = 'countExact';

    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     */
    protected $value;

    /**
     * AbstractCountConstraint constructor.
     *
     * @param string $type
     * @param int $value
     */
    public function __construct(string $type, int $value)
    {
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param ValidatableProperty $property
     *
     * @return int
     *
     * @throws Exception
     */
    protected function getCount(ValidatableProperty $property): int
    {
        $value = $property->getValue();
        if (is_object($value)) {
            if ($value instanceof Countable) {
                return $value->count();
            }

            return count(get_object_vars($value));
        }

        $this->checkType($value);

        return count($value);
    }

    /**
     * @param array|Countable|mixed $data
     *
     * @throws Exception
     */
    protected function checkType($data)
    {
        TypeValidator::validate(Type::COUNTABLE_TYPE, $data);
    }
}
