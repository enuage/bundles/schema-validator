<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation;

use ArrayObject;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;

use function in_array;

class TypeConstraint implements ValidationConstraint
{
    const CONSTRAINT_NAME = 'type';

    /**
     * @var AdvancedArrayObject
     */
    protected $types;

    /**
     * @var string|null
     */
    protected $type;

    /**
     * TypeConstraint constructor.
     *
     * @param array $types
     */
    public function __construct(array $types)
    {
        $this->types = new AdvancedArrayObject($types);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return static::CONSTRAINT_NAME;
    }

    /**
     * @inheritDoc
     *
     * @param ValidatableProperty $property
     */
    public function isValid(ValidatableProperty $property): bool
    {
        $validTypes = 0;
        foreach ($this->types as $type) {
            if (in_array($type, [Type::ARRAY_TYPE, Type::OBJECT_TYPE]) && $property->getValue() instanceof ArrayObject) {
                $validTypes++;

                continue;
            }

            if (true === TypeValidator::isValid($type, $property->getValue(), true)) {
                $validTypes++;
            }
        }

        if (0 !== $validTypes) {
            return true;
        } else {
            $this->type = $property->getType();

            return false;
        }
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return sprintf(
            'Invalid type provided for property "{propertyName}" ("{propertyType}"). Allowed types for this property: ["%s"].',
            $this->types->implode('", "')
        );
    }
}
