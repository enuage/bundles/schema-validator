<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation;

use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;

abstract class AbstractFormatConstraint implements ValidationConstraint
{
    const FORMAT_PATTERN = 'pattern';

    const FORMAT_DATE_ISO = 'date_iso';
    const FORMAT_DATE_ISO_ORDINAL = 'date_iso_ordinal';
    const FORMAT_WEEK_ISO = 'week_iso';
    const FORMAT_WEEK_ISO_WEEKDAY = 'week_iso_weekday';
    const FORMAT_DATETIME = 'datetime';
    const FORMAT_DATETIME_ISO = 'datetime_iso';

    const FORMAT_TIME = 'time';
    const FORMAT_TIME_24 = 'time_24';
    const FORMAT_TIME_24_SECONDS = 'time_24_s';
    const FORMAT_TIME_12 = 'time_12';
    const FORMAT_TIME_12_SECONDS = 'time_12_s';

    const FORMAT_TIMESTAMP = 'unix_timestamp';

    const FORMAT_URI = 'uri';
    const FORMAT_EMAIL = 'email';

    const FORMAT_IP = 'ip';
    const FORMAT_IP_V4 = 'ipv4';
    const FORMAT_IP_V6 = 'ipv6';

    const FORMAT_UUID = 'uuid';
    const FORMAT_UUID_V1 = 'uuid_v1';
    const FORMAT_UUID_V2 = 'uuid_v2';
    const FORMAT_UUID_V3 = 'uuid_v3';
    const FORMAT_UUID_V4 = 'uuid_v4';
    const FORMAT_UUID_V5 = 'uuid_v5';

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string|null
     */
    protected $pattern;

    /**
     * AbstractFormatConstraint constructor.
     *
     * @param string $type
     * @param string $pattern
     */
    public function __construct(string $type, string $pattern = null)
    {
        $this->type = $type;
        $this->pattern = $pattern;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @param ValidatableProperty $property
     *
     * @throws Exception
     */
    protected function checkType(ValidatableProperty $property)
    {
        TypeValidator::validate(Type::STRING_TYPE, $property->getValue());
    }
}
