<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Number;

use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;

class MaximalValueConstraint extends NumberValueConstraint implements ValidationConstraint
{
    /** @inheritDoc */
    public static function getName(): string
    {
        return 'maxValue';
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        TypeValidator::validate(Type::NUMBER_TYPE, $property->getValue());

        if (false === $this->isExclusive && $property->getValue() === $this->value) {
            return true;
        }

        return $this->value > $property->getValue();
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return sprintf(
            'The value of property "{propertyName}" should be greater than %s%d.',
            $this->isExclusive ? '' : 'or equal to ',
            $this->value
        );
    }
}
