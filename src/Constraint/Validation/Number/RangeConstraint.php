<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Number;

use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Enuage\Type\Helper\NumberHelper;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;

class RangeConstraint implements ValidationConstraint
{
    /**
     * @var float|int
     */
    protected $minimalValue;

    /**
     * @var float|int
     */
    protected $maximalValue;

    /**
     * RangeConstraint constructor.
     *
     * @param float|int $minimalValue
     * @param float|int $maximalValue
     *
     * @throws Exception
     */
    public function __construct($minimalValue, $maximalValue)
    {
        TypeValidator::validate(Type::NUMBER_TYPE, $minimalValue);
        TypeValidator::validate(Type::NUMBER_TYPE, $maximalValue);

        $this->minimalValue = $minimalValue;
        $this->maximalValue = $maximalValue;
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return 'range';
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        TypeValidator::validate(Type::NUMBER_TYPE, $property->getValue());

        return NumberHelper::inRange($property->getValue(), $this->minimalValue, $this->maximalValue);
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return sprintf(
            'The value of property "{propertyName}" should be in range [%d, %d].',
            $this->minimalValue,
            $this->maximalValue
        );
    }
}
