<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Number;

use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;

abstract class NumberValueConstraint
{
    /**
     * @var float|int
     */
    protected $value;

    /**
     * @var bool
     */
    protected $isExclusive;

    /**
     * NumberValueConstraint constructor.
     *
     * @param $value
     * @param bool $isExclusive
     *
     * @throws Exception
     */
    public function __construct($value, bool $isExclusive = false)
    {
        TypeValidator::validate(Type::NUMBER_TYPE, $value);

        $this->value = $value;
        $this->isExclusive = $isExclusive;
    }
}
