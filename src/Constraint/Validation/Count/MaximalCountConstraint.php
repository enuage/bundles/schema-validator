<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Count;

use Enuage\SchemaValidator\Constraint\Validation\AbstractCountConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Exception;

class MaximalCountConstraint extends AbstractCountConstraint implements ValidationConstraint
{
    /**
     * MaximalCountConstraint constructor.
     *
     * @param int $requiredValue
     */
    public function __construct(int $requiredValue)
    {
        parent::__construct(static::getName(), $requiredValue);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractCountConstraint::COUNT_MAXIMUM;
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        return $this->getValue() >= $this->getCount($property);
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return 'The count of items of property "{propertyName}" can not be more than {requiredValue} elements.';
    }
}
