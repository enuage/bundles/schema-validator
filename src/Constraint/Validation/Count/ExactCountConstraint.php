<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Count;

use Enuage\SchemaValidator\Constraint\Validation\AbstractCountConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Exception;

class ExactCountConstraint extends AbstractCountConstraint implements ValidationConstraint
{
    /**
     * ExactCountConstraint constructor.
     *
     * @param int $requiredValue
     */
    public function __construct(int $requiredValue)
    {
        parent::__construct(static::getName(), $requiredValue);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractCountConstraint::COUNT_EXACT;
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        return $this->getValue() === $this->getCount($property);
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return sprintf(
            'The count of items of property "{propertyName}" should equal to %d.',
            $this->getValue()
        );
    }
}
