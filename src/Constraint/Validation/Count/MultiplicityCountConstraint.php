<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation\Count;

use Enuage\SchemaValidator\Constraint\Validation\AbstractCountConstraint;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;
use Exception;

class MultiplicityCountConstraint extends AbstractCountConstraint implements ValidationConstraint
{
    /**
     * MultiplicityCountConstraint constructor.
     *
     * @param int $requiredValue
     */
    public function __construct(int $requiredValue)
    {
        parent::__construct(static::getName(), $requiredValue);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return AbstractCountConstraint::COUNT_MULTIPLICITY;
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function isValid(ValidatableProperty $property): bool
    {
        return 0 === ($this->getCount($property) % $this->getValue());
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return 'The count of items of property "{propertyName}" should be multiple of {requiredValue}.';
    }
}
