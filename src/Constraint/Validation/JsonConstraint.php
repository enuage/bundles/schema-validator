<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint\Validation;

use Enuage\SchemaValidator\Constraint\JsonSchema;
use Enuage\SchemaValidator\Constraint\ValidationConstraint;
use Enuage\SchemaValidator\ValidatableProperty;

use function is_object;
use function is_string;
use function json_decode;

class JsonConstraint implements ValidationConstraint
{
    /** @inheritDoc */
    public static function getName(): string
    {
        return JsonSchema::getName();
    }

    /** @inheritDoc */
    public function isValid(ValidatableProperty $property): bool
    {
        $value = $property->getValue();
        if (is_string($value)) {
            $value = json_decode($value);
            if (0 !== json_last_error()) {
                return false;
            }
        }

        return is_object($value);
    }

    /** @inheritDoc */
    public function getErrorMessage(string $type = null): string
    {
        return 'Provided data is not a valid JSON.';
    }
}
