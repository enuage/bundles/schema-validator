<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\SchemaValidator\Constraint\Validation\NotEqualConstraint;
use Enuage\Type\Helper\Type;
use Exception;

class ScalarSchema extends PropertySchema
{
    /** @inheritDoc */
    public function __construct(string $propertyName = null)
    {
        parent::__construct([Type::SCALAR_TYPE]);

        $this->propertyName = $propertyName;
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return Type::SCALAR_TYPE;
    }

    /**
     * @param $value
     *
     * @throws Exception
     */
    protected function setNotEqual($value)
    {
        $this->setValidationConstraint(new NotEqualConstraint($value));
    }
}
