<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\SchemaValidator\Constraint\Validation\TypeConstraint;
use Enuage\SchemaValidator\Factory\FormatConstraintFactory;
use Enuage\Type\Helper\Type;
use Enuage\Type\PseudoGeneric;
use Exception;

abstract class AbstractSchema
{
    /**
     * @var PseudoGeneric<int,ValidationConstraint>
     */
    protected $validationConstraints;

    /**
     * @var FormatConstraintFactory
     */
    protected $formatConstraintFactory;

    /**
     * @var bool|false
     */
    protected $required = false;

    /**
     * AbstractConstraint constructor.
     *
     * @param SchemaInterface|null $parent
     *
     * @throws Exception
     */
    public function __construct(SchemaInterface $parent = null)
    {
        $this->validationConstraints = new PseudoGeneric(ValidationConstraint::class, Type::STRING_TYPE);

        $this->formatConstraintFactory = new FormatConstraintFactory();
        if (null !== $parent) {
            $this->formatConstraintFactory = $parent->getConstraintsFactory();
        }
    }

    /**
     * Get constraint name
     *
     * @return string
     */
    abstract public static function getName(): string;

    /**
     * @param string $type
     *
     * @throws Exception
     */
    public function setType(string $type)
    {
        $this->setTypes([$type]);
    }

    /**
     * @param array $types
     *
     * @throws Exception
     */
    public function setTypes(array $types)
    {
        $this->setValidationConstraint(new TypeConstraint($types));
    }

    /**
     * @param ValidationConstraint $validationConstraint
     *
     * @return self
     *
     * @throws Exception
     */
    protected function setValidationConstraint(ValidationConstraint $validationConstraint): self
    {
        $this->validationConstraints->set($validationConstraint::getName(), $validationConstraint);

        return $this;
    }

    /**
     * @return PseudoGeneric
     */
    public function getValidationConstraints(): PseudoGeneric
    {
        return $this->validationConstraints;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }
}
