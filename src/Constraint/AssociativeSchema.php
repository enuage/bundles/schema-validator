<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\Type\AdvancedArrayObject;
use Exception;
use Ramsey\Uuid\Uuid;

abstract class AssociativeSchema extends Schema
{
    /**
     * @var AdvancedArrayObject<PropertySchemaInterface>
     */
    protected $properties;

    /**
     * @var bool
     */
    protected $additionalPropertiesAllowed = false;

    /**
     * @var AdvancedArrayObject|null
     */
    protected $additionalProperties;

    /**
     * @param string $propertyName
     *
     * @return ArraySchema
     *
     * @throws Exception
     */
    public function addArrayProperty(string $propertyName): ArraySchema
    {
        /** @var ArraySchema $constraint */
        $constraint = $this->createConstraint(ArraySchema::getName());
        $constraint->setPropertyName($propertyName);

        $this->addProperty($constraint);

        return $constraint;
    }

    /**
     * @param string $propertyName
     *
     * @return AssociativeArraySchema
     *
     * @throws Exception
     */
    public function addAssociativeArrayProperty(string $propertyName): AssociativeArraySchema
    {
        /** @var AssociativeArraySchema $constraint */
        $constraint = $this->createConstraint(AssociativeArraySchema::getName());
        $constraint->setPropertyName($propertyName);

        $this->addProperty($constraint);

        return $constraint;
    }

    /**
     * @param AbstractSchema $propertyConstraint
     *
     * @return Schema
     */
    public function addProperty(AbstractSchema $propertyConstraint): Schema
    {
        $propertyName = Uuid::uuid4()->toString();
        if ($propertyConstraint instanceof PropertySchemaInterface) {
            $propertyName = $propertyConstraint->getPropertyName() ?? $propertyName;
        }

        $this->getProperties()->set($propertyName, $propertyConstraint);

        return $this;
    }

    /**
     * @return AdvancedArrayObject
     */
    public function getProperties(): AdvancedArrayObject
    {
        return $this->properties;
    }

    /**
     * @param string $propertyName
     *
     * @return ObjectSchema
     *
     * @throws Exception
     */
    public function addObjectProperty(string $propertyName): ObjectSchema
    {
        /** @var ObjectSchema $constraint */
        $constraint = $this->createConstraint(ObjectSchema::getName());
        $constraint->setPropertyName($propertyName);

        $this->addProperty($constraint);

        return $constraint;
    }

    /**
     * @param string $propertyName
     *
     * @return ScalarSchema
     *
     * @throws Exception
     */
    public function addScalarProperty(string $propertyName): ScalarSchema
    {
        /** @var ScalarSchema $constraint */
        $constraint = $this->createConstraint(ScalarSchema::getName());
        $constraint->setPropertyName($propertyName);

        $this->addProperty($constraint);

        return $constraint;
    }

    /**
     * @param string $propertyName
     *
     * @return BooleanSchema
     *
     * @throws Exception
     */
    public function addBooleanProperty(string $propertyName): BooleanSchema
    {
        /** @var BooleanSchema $constraint */
        $constraint = $this->createConstraint(BooleanSchema::getName());
        $constraint->setPropertyName($propertyName);

        $this->addProperty($constraint);

        return $constraint;
    }

    /**
     * @param string $propertyName
     *
     * @return StringSchema
     *
     * @throws Exception
     */
    public function addStringProperty(string $propertyName): StringSchema
    {
        /** @var StringSchema $constraint */
        $constraint = $this->createConstraint(StringSchema::getName());
        $constraint->setPropertyName($propertyName);

        $this->addProperty($constraint);

        return $constraint;
    }

    /**
     * @param string $propertyName
     *
     * @return NumberSchema
     *
     * @throws Exception
     */
    public function addNumberProperty(string $propertyName): NumberSchema
    {
        /** @var NumberSchema $constraint */
        $constraint = $this->createConstraint(NumberSchema::getName());
        $constraint->setPropertyName($propertyName);

        $this->addProperty($constraint);

        return $constraint;
    }

    /**
     * @param string $propertyName
     *
     * @return FloatSchema
     *
     * @throws Exception
     */
    public function addFloatProperty(string $propertyName): FloatSchema
    {
        /** @var FloatSchema $constraint */
        $constraint = $this->createConstraint(FloatSchema::getName());
        $constraint->setPropertyName($propertyName);

        $this->addProperty($constraint);

        return $constraint;
    }

    /**
     * @param string $propertyName
     *
     * @return IntegerSchema
     *
     * @throws Exception
     */
    public function addIntegerProperty(string $propertyName): IntegerSchema
    {
        /** @var IntegerSchema $constraint */
        $constraint = $this->createConstraint(IntegerSchema::getName());
        $constraint->setPropertyName($propertyName);

        $this->addProperty($constraint);

        return $constraint;
    }

    /**
     * @param bool|array|string[]|PropertySchemaInterface[] $value
     *
     * @return self
     */
    public function additionalProperties($value): self
    {
        if (is_bool($value)) {
            $this->additionalPropertiesAllowed = true === $value;
        }

        if (is_array($value)) {
            $additionalProperties = new AdvancedArrayObject();
            foreach ($value as $index => $item) {
                if (is_string($item)) {
                    $additionalProperties->set($item, $item);
                }

                if ($item instanceof PropertySchemaInterface) {
                    $additionalProperties->set(is_string($index) ? $index : $item->getPropertyName(), $item);
                }
            }

            if (false === $additionalProperties->isEmpty()) {
                $this->additionalPropertiesAllowed = true;
                $this->additionalProperties = $additionalProperties;
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdditionalPropertiesAllowed(): bool
    {
        return $this->additionalPropertiesAllowed;
    }

    /**
     * @return AdvancedArrayObject|null
     */
    public function getAdditionalProperties()
    {
        return $this->additionalProperties;
    }

    /**
     * @param string $title
     * @param string $reference
     *
     * @return self
     *
     * @throws Exception
     */
    public function addByReference(string $title, string $reference): self
    {
        if($this->getDefinitions()->containsKey($reference)) {
            $this->getProperties()->set($title, $this->getDefinitions()->get($reference));
        }

        return $this;
    }
}
