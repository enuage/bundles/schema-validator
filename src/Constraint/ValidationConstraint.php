<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\SchemaValidator\ValidatableProperty;

interface ValidationConstraint
{
    /**
     * @return string
     */
    public static function getName(): string;

    /**
     * @param ValidatableProperty $property
     *
     * @return bool
     */
    public function isValid(ValidatableProperty $property): bool;

    /**
     * @param string|null $type
     *
     * @return string
     */
    public function getErrorMessage(string $type = null): string;
}
