<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\Type\Helper\Type;
use Exception;

class BooleanSchema extends ScalarSchema
{
    /** @inheritDoc */
    public function __construct(string $propertyName = null)
    {
        parent::__construct($propertyName);

        $this->setType(Type::BOOLEAN_TYPE);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return Type::BOOLEAN_TYPE;
    }

    /**
     * @param bool $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function neq(bool $value): self
    {
        return $this->notEqualTo($value);
    }

    /**
     * @param bool $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function notEqualTo(bool $value): self
    {
        parent::setNotEqual($value);

        return $this;
    }
}
