<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\SchemaValidator\Constraint\Validation\AbstractFormatConstraint;
use Enuage\SchemaValidator\Constraint\Validation\EnumConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Length\MaximalLengthConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Length\MinimalLengthConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Length\RequiredLengthConstraint;
use Enuage\SchemaValidator\Constraint\Validation\NotEmptyConstraint;
use Enuage\Type\Helper\Type;
use Enuage\Type\Validator\TypeValidator;
use Exception;

class StringSchema extends ScalarSchema
{
    /** @inheritDoc */
    public function __construct(string $propertyName = null)
    {
        parent::__construct($propertyName);

        $this->setType(Type::STRING_TYPE);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return Type::STRING_TYPE;
    }

    /**
     * @param int $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function minLength(int $value): self
    {
        $this->setValidationConstraint(new MinimalLengthConstraint($value));

        return $this;
    }

    /**
     * @param int $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function maxLength(int $value): self
    {
        $this->setValidationConstraint(new MaximalLengthConstraint($value));

        return $this;
    }

    /**
     * @param int $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function requiredLength(int $value): self
    {
        $this->setValidationConstraint(new RequiredLengthConstraint($value));

        return $this;
    }

    /**
     * @param array|string[] $values
     *
     * @return self
     *
     * @throws Exception
     */
    public function enum(array $values): self
    {
        TypeValidator::validateArray(Type::STRING_TYPE, $values);

        $this->setValidationConstraint(new EnumConstraint($values));

        return $this;
    }

    /**
     * @param string $pattern
     *
     * @return self
     *
     * @throws Exception
     */
    public function pattern(string $pattern): self
    {
        $this->setValidationConstraint(
            $this->formatConstraintFactory->createConstraint(AbstractFormatConstraint::FORMAT_PATTERN, [$pattern])
        );

        return $this;
    }

    /**
     * @param bool $notEmpty
     *
     * @return self
     *
     * @throws Exception
     */
    public function notEmpty(bool $notEmpty = true): self
    {
        $this->setValidationConstraint(new NotEmptyConstraint(false === $notEmpty));

        return $this;
    }

    /**
     * @param string $format
     *
     * @return self
     *
     * @throws Exception
     */
    public function format(string $format): self
    {
        $this->setValidationConstraint($this->formatConstraintFactory->createConstraint($format));

        return $this;
    }

    /**
     * Alias for `notEqualTo`
     *
     * @param string $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function neq(string $value): self
    {
        return $this->notEqualTo($value);
    }

    /**
     * @param string $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function notEqualTo(string $value): self
    {
        parent::setNotEqual($value);

        return $this;
    }
}
