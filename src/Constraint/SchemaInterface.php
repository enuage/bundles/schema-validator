<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\SchemaValidator\Factory\ConstraintsFactory;
use Enuage\SchemaValidator\Factory\FormatConstraintFactory;

interface SchemaInterface
{
    /**
     * @return ConstraintsFactory|null
     */
    public function getConstraintsFactory();

    /**
     * @param FormatConstraintFactory $formatConstraintFactory
     *
     * @return self
     */
    public function setFormatConstraintFactory(FormatConstraintFactory $formatConstraintFactory);

    /**
     * @return FormatConstraintFactory
     */
    public function getFormatConstraintFactory(): FormatConstraintFactory;
}
