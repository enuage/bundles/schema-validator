<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\SchemaValidator\Constraint\Validation\JsonConstraint;
use Enuage\SchemaValidator\Parser\SchemaParser;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Helper\Type;
use Exception;
use InvalidArgumentException;
use RuntimeException;

use function json_decode;
use function json_last_error;

class JsonSchema extends ObjectSchema implements ParsableSchema
{
    /** @inheritDoc */
    public function __construct(string $title = null, SchemaInterface $parent = null)
    {
        parent::__construct($title, $parent);

        $this->setTypes([Type::STRING_TYPE, Type::OBJECT_TYPE]);
        $this->setValidationConstraint(new JsonConstraint());
    }

    /**
     * @param string $configuration
     *
     * @return JsonSchema
     *
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public static function fromString(string $configuration): JsonSchema
    {
        $configuration = json_decode($configuration, true);
        if (0 !== json_last_error()) {
            throw new InvalidArgumentException('Invalid JSON provided.');
        }

        $configuration = new AdvancedArrayObject($configuration);
        $jsonSchema = new JsonSchema($configuration->get(SchemaParser::PROPERTY_TITLE, null));

        return SchemaParser::fromArray($jsonSchema, $configuration);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return 'json';
    }
}
