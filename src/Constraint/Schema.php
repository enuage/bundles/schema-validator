<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\SchemaValidator\Constraint\Validation\Count\ExactCountConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Count\MaximalCountConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Count\MinimalCountConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Count\MultiplicityCountConstraint;
use Enuage\SchemaValidator\Constraint\Validation\NotEmptyConstraint;
use Enuage\SchemaValidator\Factory\ConstraintsFactory;
use Enuage\SchemaValidator\Factory\FormatConstraintFactory;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Helper\Type;
use Enuage\Type\PseudoGeneric;
use Enuage\Type\Validator\TypeValidator;
use Exception;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid;

use function array_unique;
use function array_values;
use function class_exists;
use function is_string;

class Schema extends AbstractSchema implements SchemaInterface, PropertySchemaInterface
{
    const CONSTRAINT_NAME = 'schema';

    /**
     * @var string
     */
    protected $propertyName;

    /**
     * @var ConstraintsFactory
     */
    protected $constraintsFactory;

    /**
     * @var AdvancedArrayObject|null
     */
    protected $items;

    /**
     * @var AbstractSchema|null
     */
    protected $itemsSchema;

    /**
     * @var AdvancedArrayObject|null
     */
    protected $requiredItems;

    /**
     * @var PseudoGeneric
     */
    protected $definitions;

    /**
     * SchemaConstraint constructor.
     *
     * @param string $title
     * @param SchemaInterface|null $parent
     * @param ConstraintsFactory|null $constraintsFactory
     *
     * @throws Exception
     */
    public function __construct(
        string $title = null,
        SchemaInterface $parent = null,
        ConstraintsFactory $constraintsFactory = null
    ) {
        parent::__construct($parent);

        $this->propertyName = $title ?? Uuid::uuid4()->toString();
        $this->setType(Type::ARRAY_OBJECT_TYPE);

        if (null === $constraintsFactory) {
            $constraintsFactory = $parent ? $parent->getConstraintsFactory() : new ConstraintsFactory();
        }
        $this->setConstraintsFactory($constraintsFactory);

        $this->definitions = new PseudoGeneric(AbstractSchema::class, Type::STRING_TYPE);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return static::CONSTRAINT_NAME;
    }

    /** @inheritDoc */
    public function getPropertyName(): string
    {
        return $this->propertyName ?? Uuid::uuid4()->toString();
    }

    /** @inheritDoc */
    public function setPropertyName(string $propertyName)
    {
        $this->propertyName = $propertyName;
    }

    /**
     * @param int $multiplicityCount
     *
     * @return self
     *
     * @throws Exception
     */
    public function multipleOf(int $multiplicityCount): self
    {
        $this->setValidationConstraint(new MultiplicityCountConstraint($multiplicityCount));

        return $this;
    }

    /**
     * @param bool $notEmpty
     *
     * @return self
     *
     * @throws Exception
     */
    public function notEmpty(bool $notEmpty = true): self
    {
        $this->setValidationConstraint(new NotEmptyConstraint(false === $notEmpty));

        return $this;
    }

    /**
     * @param FormatConstraintFactory $formatConstraintFactory
     *
     * @return self
     */
    public function setFormatConstraintFactory(FormatConstraintFactory $formatConstraintFactory): self
    {
        $this->formatConstraintFactory = $formatConstraintFactory;

        return $this;
    }

    /**
     * @return FormatConstraintFactory
     */
    public function getFormatConstraintFactory(): FormatConstraintFactory
    {
        return $this->formatConstraintFactory;
    }

    /**
     * @param bool $isRequired
     *
     * @return self
     */
    public function setRequired(bool $isRequired = true): self
    {
        $this->required = $isRequired;

        return $this;
    }

    /**
     * @param array $items
     *
     * @return self
     *
     * @throws Exception
     */
    public function items(array $items): self
    {
        $this->items = new AdvancedArrayObject();
        foreach ($items as $index => $item) {
            if ($item instanceof PropertySchemaInterface) {
                $this->items->set(is_string($index) ? $index : $item->getPropertyName(), $item);
            } else {
                TypeValidator::validate(Type::SCALAR_TYPE, $item);

                $this->items->set(is_string($index) ? $index : strval($item), $item);
            }
        }

        return $this;
    }

    /**
     * @return AdvancedArrayObject|null
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return AbstractSchema|null
     */
    public function getItemsSchema()
    {
        return $this->itemsSchema;
    }

    /**
     * @param string|AbstractSchema $schema
     *
     * @return self
     *
     * @throws Exception
     */
    public function oneOf($schema): self
    {
        if(is_string($schema) && $this->getDefinitions()->containsKey($schema)) {
            $schema = $this->getDefinitions()->get($schema);
        }

        $this->exactOf($schema, 1);

        return $this;
    }

    /**
     * @param string|AbstractSchema $schema
     * @param int $requiredCount
     *
     * @return self
     *
     * @throws Exception
     */
    public function exactOf($schema, int $requiredCount): self
    {
        if(is_string($schema) && $this->getDefinitions()->containsKey($schema)) {
            $schema = $this->getDefinitions()->get($schema);
        }

        $this->allOf($schema);
        $this->setExactCount($requiredCount);

        return $this;
    }

    /**
     * @param string|AbstractSchema $schema
     *
     * @return self
     *
     * @throws Exception
     */
    public function allOf($schema): self
    {
        if (is_string($schema)) {
            if ($this->getDefinitions()->containsKey($schema)) {
                $schema = $this->getDefinitions()->get($schema);
            } else {
                $schema = $this->createConstraint($schema);
            }
        }

        if (false === ($schema instanceof AbstractSchema)) {
            throw new InvalidArgumentException('Invalid schema provided for method `allOf(type|AbstractSchema)`');
        }

        $this->itemsSchema = $schema;

        return $this;
    }

    /**
     * @param string $type
     *
     * @return AbstractSchema
     *
     * @throws Exception
     */
    protected function createConstraint(string $type): AbstractSchema
    {
        return $this->getConstraintsFactory()->createConstraint($type, $this);
    }

    /** @inheritDoc */
    public function getConstraintsFactory(): ConstraintsFactory
    {
        return $this->constraintsFactory;
    }

    /**
     * @param ConstraintsFactory|string $constraintsFactory
     *
     * @return self
     *
     * @throws InvalidArgumentException
     */
    public function setConstraintsFactory($constraintsFactory): self
    {
        if (is_string($constraintsFactory)) {
            if (!class_exists($constraintsFactory)) {
                throw new InvalidArgumentException('Class "'.$constraintsFactory.'" does not exists.');
            }

            $constraintsFactory = new $constraintsFactory;
        }

        if (!($constraintsFactory instanceof ConstraintsFactory)) {
            throw new InvalidArgumentException(
                'Constraints factory should be compatible with class "'.ConstraintsFactory::class.'".'
            );
        }

        $this->constraintsFactory = $constraintsFactory;

        return $this;
    }

    /**
     * @param int $exactCount
     *
     * @return self
     *
     * @throws Exception
     */
    protected function setExactCount(int $exactCount): self
    {
        $this->setValidationConstraint(new ExactCountConstraint($exactCount));

        return $this;
    }

    /**
     * @param array $properties
     *
     * @return self
     *
     * @throws Exception
     */
    public function required(array $properties): self
    {
        TypeValidator::validateArray(Type::STRING_TYPE, $properties);

        if (!empty($properties)) {
            $this->requiredItems = new AdvancedArrayObject(array_unique(array_values($properties)));
        }

        return $this;
    }

    /**
     * @return AdvancedArrayObject|null
     */
    public function getRequiredItems()
    {
        return $this->requiredItems;
    }

    /**
     * @return PseudoGeneric
     */
    public function getDefinitions(): PseudoGeneric
    {
        return $this->definitions;
    }

    /**
     * @param array $definitions
     *
     * @return self
     *
     * @throws Exception
     */
    public function definitions(array $definitions): self
    {
        foreach ($definitions as $title => $schema) {
            $this->addDefinition($title, $schema);
        }

        return $this;
    }

    /**
     * @param string $title
     * @param AbstractSchema $schema
     *
     * @return self
     *
     * @throws Exception
     */
    public function addDefinition(string $title, AbstractSchema $schema): self
    {
        $this->definitions->set($title, $schema);

        return $this;
    }

    /**
     * @param int $minimalCount
     *
     * @return self
     *
     * @throws Exception
     */
    protected function setMinCount(int $minimalCount): self
    {
        $this->setValidationConstraint(new MinimalCountConstraint($minimalCount));

        return $this;
    }

    /**
     * @param int $maximalCount
     *
     * @return self
     *
     * @throws Exception
     */
    protected function setMaxCount(int $maximalCount): self
    {
        $this->setValidationConstraint(new MaximalCountConstraint($maximalCount));

        return $this;
    }
}
