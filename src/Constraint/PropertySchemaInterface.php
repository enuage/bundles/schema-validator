<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

interface PropertySchemaInterface
{
    /**
     * @param string $propertyName
     *
     * @return self
     */
    public function setPropertyName(string $propertyName);

    /**
     * @return string
     */
    public function getPropertyName(): string;
}
