<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\SchemaValidator\Constraint\Validation\UniqueItemsConstraint;
use Enuage\Type\Helper\Type;
use Enuage\Type\PseudoGeneric;
use Exception;

class AssociativeArraySchema extends AssociativeSchema
{
    /** @inheritDoc */
    public function __construct(string $title = null, SchemaInterface $parent = null)
    {
        parent::__construct($title, $parent);

        $this->setType(Type::ARRAY_TYPE);
        $this->properties = new PseudoGeneric(AbstractSchema::class, Type::STRING_TYPE);
    }

    /**
     * @param ArraySchema $arraySchema
     *
     * @return AssociativeArraySchema
     */
    public function cloneArraySchema(ArraySchema $arraySchema): self
    {
        $this->constraintsFactory = $arraySchema->getConstraintsFactory();
        $this->formatConstraintFactory = $arraySchema->getFormatConstraintFactory();
        $this->items = $arraySchema->getItems();
        $this->itemsSchema = $arraySchema->getItemsSchema();
        $this->validationConstraints = $arraySchema->getValidationConstraints();
        $this->definitions = $arraySchema->getDefinitions();
        $this->requiredItems = $arraySchema->getRequiredItems();
        $this->required = $arraySchema->isRequired();

        return $this;
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return 'array_associative';
    }

    /**
     * @param int $minProperties
     *
     * @return self
     *
     * @throws Exception
     */
    public function minItems(int $minProperties): self
    {
        $this->setMinCount($minProperties);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function maxItems(int $value): self
    {
        $this->setMaxCount($value);

        return $this;
    }

    /**
     * @param bool $unique
     *
     * @return self
     *
     * @throws Exception
     */
    public function uniqueItems(bool $unique = true): self
    {
        if (true === $unique) {
            $this->setValidationConstraint(new UniqueItemsConstraint());
        }

        return $this;
    }
}
