<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\Type\Helper\Type;
use Exception;

class IntegerSchema extends NumberSchema
{
    /** @inheritDoc */
    public function __construct(string $propertyName = null)
    {
        parent::__construct($propertyName);

        $this->setType(Type::INTEGER_TYPE);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return Type::INTEGER_TYPE;
    }

    /**
     * Alias for `notEqualTo`
     *
     * @param int $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function neq(int $value): self
    {
        return $this->notEqualTo($value);
    }

    /**
     * @param int $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function notEqualTo(int $value): self
    {
        parent::setNotEqual($value);

        return $this;
    }

    /**
     * Alias for `exclusiveMinimum`. Property value SHOULD be greater than defined value. `gt` = `greater than`
     *
     * @link https://tools.ietf.org/html/draft-wright-json-schema-validation-00#page-6
     *
     * @param int $minimalValue
     *
     * @return self
     *
     * @throws Exception
     */
    public function gt(int $minimalValue): self
    {
        return $this->exclusiveMinimum($minimalValue);
    }

    /**
     * Property value SHOULD be greater than defined value.
     *
     * @link https://tools.ietf.org/html/draft-wright-json-schema-validation-00#page-6
     *
     * @param int $minimalValue
     *
     * @return self
     *
     * @throws Exception
     */
    public function exclusiveMinimum(int $minimalValue): self
    {
        return $this->minimum($minimalValue, true);
    }

    /**
     * @param int $minimalValue
     * @param bool $isExclusive
     *
     * @return self
     *
     * @throws Exception
     */
    public function minimum(int $minimalValue, bool $isExclusive = false): self
    {
        parent::setMinValue($minimalValue, $isExclusive);

        return $this;
    }

    /**
     * Alias for `minimum`. Provided value SHOULD be grater than or equal to defined value. `gte` = greater than or equal
     *
     * @link https://tools.ietf.org/html/draft-wright-json-schema-validation-00#page-6
     *
     * @param int $minimalValue
     *
     * @return self
     *
     * @throws Exception
     */
    public function gte(int $minimalValue): self
    {
        return $this->minimum($minimalValue);
    }

    /**
     * Alias for `exclusiveMaximum`. Provided value SHOULD be less than defined value. `lt` = less than
     *
     * @link https://tools.ietf.org/html/draft-wright-json-schema-validation-00#page-6
     *
     * @param int $maximalValue
     *
     * @return self
     *
     * @throws Exception
     */
    public function lt(int $maximalValue): self
    {
        return $this->exclusiveMaximum($maximalValue);
    }

    /**
     * Provided value SHOULD be less than defined value.
     *
     * @link https://tools.ietf.org/html/draft-wright-json-schema-validation-00#page-6
     *
     * @param int $maximumValue
     *
     * @return self
     *
     * @throws Exception
     */
    public function exclusiveMaximum(int $maximumValue): self
    {
        return $this->maximum($maximumValue, true);
    }

    /**
     * @param int $maximalValue
     * @param bool $isExclusive
     *
     * @return self
     *
     * @throws Exception
     */
    public function maximum(int $maximalValue, bool $isExclusive = false): self
    {
        parent::setMaxValue($maximalValue, $isExclusive);

        return $this;
    }

    /**
     * Alias for `maximum`. Provided value SHOULD be less than or equal to provided value. `lte` = less than or equal
     *
     * @link https://tools.ietf.org/html/draft-wright-json-schema-validation-00#page-6
     *
     * @param int $maximalValue
     *
     * @return self
     *
     * @throws Exception
     */
    public function lte(int $maximalValue): self
    {
        return $this->maximum($maximalValue);
    }

    /**
     * @param int $minimalValue
     * @param int $maximalValue
     *
     * @return self
     *
     * @throws Exception
     */
    public function range(int $minimalValue, int $maximalValue): self
    {
        parent::setRange($minimalValue, $maximalValue);

        return $this;
    }
}
