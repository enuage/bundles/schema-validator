<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\SchemaValidator\Constraint\Validation\Number\MaximalValueConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Number\MinimalValueConstraint;
use Enuage\SchemaValidator\Constraint\Validation\Number\RangeConstraint;
use Enuage\SchemaValidator\Constraint\Validation\PositiveConstraint;
use Enuage\Type\Helper\Type;
use Exception;

class NumberSchema extends ScalarSchema
{
    /** @inheritDoc */
    public function __construct(string $propertyName = null)
    {
        parent::__construct($propertyName);

        $this->setType(Type::NUMBER_TYPE);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return Type::NUMBER_TYPE;
    }

    /**
     * @return self
     *
     * @throws Exception
     */
    public function positive(): self
    {
        $this->setValidationConstraint(new PositiveConstraint(true));

        return $this;
    }

    /**
     * @return self
     *
     * @throws Exception
     */
    public function negative(): self
    {
        $this->setValidationConstraint(new PositiveConstraint(false));

        return $this;
    }

    /**
     * @param $value
     * @param bool $isExclusive
     *
     * @throws Exception
     */
    protected function setMaxValue($value, bool $isExclusive = false)
    {
        $this->setValidationConstraint(new MaximalValueConstraint($value, $isExclusive));
    }

    /**
     * @param $value
     * @param bool $isExclusive
     *
     * @throws Exception
     */
    protected function setMinValue($value, bool $isExclusive = false)
    {
        $this->setValidationConstraint(new MinimalValueConstraint($value, $isExclusive));
    }

    /**
     * @param float|int $minimalValue
     * @param float|int $maximalValue
     *
     * @throws Exception
     */
    protected function setRange($minimalValue, $maximalValue)
    {
        $this->setValidationConstraint(new RangeConstraint($minimalValue, $maximalValue));
    }
}
