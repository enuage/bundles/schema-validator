<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\Type\Helper\Type;
use Enuage\Type\PseudoGeneric;
use Exception;

class ObjectSchema extends AssociativeSchema
{
    /** @inheritDoc */
    public function __construct(string $title = null, SchemaInterface $parent = null)
    {
        parent::__construct($title, $parent);

        $this->setType(Type::OBJECT_TYPE);
        $this->properties = new PseudoGeneric(AbstractSchema::class, Type::STRING_TYPE);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return Type::OBJECT_TYPE;
    }

    /**
     * @param int $minProperties
     *
     * @return self
     *
     * @throws Exception
     */
    public function minProperties(int $minProperties): self
    {
        $this->setMinCount($minProperties);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return self
     *
     * @throws Exception
     */
    public function maxProperties(int $value): self
    {
        $this->setMaxCount($value);

        return $this;
    }
}
