<?php

declare(strict_types=1);

namespace Enuage\SchemaValidator\Constraint;

use Enuage\Type\Helper\Type;
use Exception;

class SchemaPrototype extends AbstractSchema
{
    const CONSTRAINT_NAME = 'prototype';

    /**
     * PrototypeConstraint constructor.
     *
     * @param array $requiredTypes
     *
     * @throws Exception
     */
    public function __construct(array $requiredTypes = [Type::MIXED_TYPE])
    {
        parent::__construct();

        $this->setTypes($requiredTypes);
    }

    /** @inheritDoc */
    public static function getName(): string
    {
        return static::CONSTRAINT_NAME;
    }

    /** @inheritDoc */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @param bool $isRequired
     *
     * @return self
     */
    public function required(bool $isRequired = true): self
    {
        $this->required = $isRequired;

        return $this;
    }
}
