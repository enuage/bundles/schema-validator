### Validator

Schema:

https://tools.ietf.org/html/draft-wright-json-schema-validation-00

- [x] title(string)
- [x] notEmpty(boolean)
- [x] multipleOf(integer)
- [x] items(string[]|Schema[])
- [x] allOf(string|Schema)
- [x] oneOf(string|Schema)
- [x] exactOf(string|Schema, integer)
- [x] definitions(array<string, Schema>)
- [x] required(string[])

Array (inherit of schema):

- [x] maxItems(integer)
- [x] minItems(integer)
- [x] uniqueItems(boolean)
- [x] associative(): AssociativeArraySchema

The associative schema (inherit of schema)

- [x] addArrayProperty(string)
- [x] addProperty(PropertySchemaInterface)
- [x] addObjectProperty(string)
- [x] addScalarProperty(string)
- [x] addBooleanProperty(string)
- [x] addStringProperty(string)
- [x] addNumberProperty(string)
- [x] addFloatProperty(string)
- [x] addIntegerProperty(string)
- [x] additionalProperties(boolean|string[]|Schema[])
- [x] addByReference(string, string)

The associative array (inherit of associative schema)

- [x] maxItems(integer)
- [x] minItems(integer)
- [x] uniqueItems(boolean)

Object (inherit of associative schema):

- [x] minProperties(integer)
- [x] maxProperties(integer)

JSON (inherit of object)

- [x] (static) fromString(string)

Number:

- [x] positive(void)
- [x] negative(void)

Float (inherit of Number):

- [x] notEqualTo(float) // alias = `neq`
- [x] exclusiveMinimum(float) // alias = `gt`
- [x] minimum(float) // alias = `gte`
- [x] exclusiveMaximum(float) // alias = `lt`
- [x] maximum(float) // alias = `lte`
- [x] range(float, float)

Integer (inherit of Number):

- [x] notEqualTo(integer) // alias = `neq`
- [x] exclusiveMinimum(integer) // alias = `gt`
- [x] minimum(integer) // alias = `gte`
- [x] exclusiveMaximum(integer) // alias = `lt`
- [x] maximum(integer) // alias = `lte`
- [x] range(integer, integer)

String:

- [x] minLength(integer)
- [x] maxLength(integer)
- [x] requiredLength(integer)
- [x] enum(array)
- [x] pattern(string)
- [x] notEmpty(boolean)
- [x] format(string)
    - [ ] numberFormat(string)
    - [x] date & time format
    - [x] URI
    - [x] E-mail address
    - [x] IP address
    - [x] UUID
- [x] notEqualTo(string) // alias = `neq`

### Parsers

- [x] JSON
- [ ] YAML
- [ ] XML
- [ ] Array
    - [ ] simplified (inline rules)

### JSON schema validation specification

- [x] Adding by reference
