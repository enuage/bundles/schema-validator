# Schema validation

License: [GNU GPL v3 or later](https://www.gnu.org/licenses/gpl-3.0.en.html)

Status: `abandoned`

Please use https://github.com/opis/json-schema instead

## Requirements

- PHP >= 7.0
- [PHP JSON extension](https://www.php.net/manual/en/book.json.php)
- [PHP internationalization extension](https://www.php.net/manual/en/book.intl.php)
- Composer

## Installation

```
composer require enuage/schema-validator
```

## Usage

### Manual configuration

```php
<?php

use Enuage\SchemaValidator\Constraint\ObjectSchema;
use Enuage\SchemaValidator\Throwable\SchemaValidationException;
use Enuage\SchemaValidator\Validator\SchemaValidator;

$schema = new ObjectSchema('schema');
$schema->addStringProperty('string');
$schema->addIntegerProperty('integer');
$schema->addArrayProperty('array')->associative()->addStringProperty('string');

$result = SchemaValidator::validateData([
    'string' => 'Lorem ipsum dolor sit amet',
    'integer' => 42,
    'array' => [
        'string' => 'Hello world',
    ],
], $schema);

if (true === $result->isValid()) {
    // Do something
} else {
    /** @var SchemaValidationException $error */
    foreach ($result->getViolations() as $error) {
        $message = $error->getMessage();
    }
}
```

### Configuration from file

```php
<?php

use Enuage\SchemaValidator\Constraint\JsonSchema;
use Enuage\SchemaValidator\Validator\SchemaValidator;

$structure = JsonSchema::fromString('
{
    "title": "schema",
    "properties": {
        "string": {
            "type": "string"
        },
        "integer": {
            "type": "integer"
        },
        "array": {
            "title": "loremIpsum",
            "type": "array",
            "associative": true,
            "properties": {
                "string": {
                    "type": "string"
                }
            }
        },
    }
}
');

$result = SchemaValidator::validateData([
    'string' => 'Lorem ipsum dolor sit amet',
    'integer' => 42,
    'loremIpsum' => [
        'string' => 'Hello world',
    ],
], $structure);
```
